import React, {useEffect, useState} from 'react';
import { ThemeProvider } from "@mui/styles";
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import {useParams} from "react-router-dom";

export default function DetailHealthStatus() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [medicalCheckups, setMedicalCheckups] = useState([]);

    let { status } = useParams();

    useEffect(() => {
        getData();
    }, []);

    let theme = createTheme();
    theme = responsiveFontSizes(theme);

    const columns = [
        {
            name: "nik",
            label: "NIK",
            options: {
                filter: true,
                sort: false
            }
        },
        {
            name: "fullname",
            label: "Nama Lengkap",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "birthdate",
            label: "Tanggal Lahir",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "division",
            label: "Divisi",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "height",
            label: "Tinggi Badan",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "weight",
            label: "Berat Badan",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "sistole",
            label: "Tekanan Darah",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "blood_sugar_level",
            label: "Gula Darah",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "cholesterol_level",
            label: "Kolesterol",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "uric_acid_level",
            label: "Asam Urat",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "checkup_date",
            label: "Tanggal Medical Checkup",
            options: {
                filter: true,
                sort: false,
            }
        },
    ];

    const options = {
        filterType: 'checkbox',
        selectableRows: false
    };

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
            .then(res => {

                var healthStatus;

                if (status === 'Kolesterol') {
                    healthStatus = 'cholesterol';
                } else if (status === 'Hipertensi Stage 1') {
                    healthStatus = 'stageOneHypertension';
                } else if (status === 'Hipertensi Stage 2') {
                    healthStatus = 'stageTwoHypertension';
                } else if (status === 'Asam Urat') {
                    healthStatus = 'uricAcid';
                } else if (status === 'Gula Darah') {
                    healthStatus = 'bloodSugar';
                }

                const params = {
                    companyId: res.data.data.companyId,
                    status: healthStatus
                }

                axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/status`, { params })
                    .then(res => {
                        setMedicalCheckups(res.data.data)
                    })
            })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
            {/* Sidebar */}
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            {/* Content area */}
            <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                {/*  Site header */}
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} useBack={true} />
                <main>
                    <div className="col-span-full xl:col-span-6 rounded-lg">
                        <div className="p-3">
                            {/*<ThemeProvider theme={theme}>*/}
                            {/*    <MUIDataTable*/}
                            {/*        title={`Data ${status}`}*/}
                            {/*        data={medicalCheckups.map(medicalCheckup => {*/}
                            {/*            return [*/}
                            {/*                medicalCheckup.nik,*/}
                            {/*                medicalCheckup.fullname,*/}
                            {/*                medicalCheckup.birthdate,*/}
                            {/*                medicalCheckup.division,*/}
                            {/*                medicalCheckup.master_medical_checkup.height,*/}
                            {/*                medicalCheckup.master_medical_checkup.weight,*/}
                            {/*                medicalCheckup.master_medical_checkup.sistole,*/}
                            {/*                medicalCheckup.master_medical_checkup.blood_sugar_level,*/}
                            {/*                medicalCheckup.master_medical_checkup.cholesterol_level,*/}
                            {/*                medicalCheckup.master_medical_checkup.uric_acid_level,*/}
                            {/*                medicalCheckup.master_medical_checkup.checkup_date*/}
                            {/*            ]*/}
                            {/*        })}*/}
                            {/*        columns={columns}*/}
                            {/*        options={options}*/}
                            {/*    />*/}
                            {/*</ThemeProvider>*/}
                        </div>
                    </div>
                </main>
            </div>
        </div>
    )
}