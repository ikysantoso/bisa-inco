import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";
import axios from 'axios';

import Sidebar from '../partials/Sidebar';
import Header from '../partials/Header';
import Banner from '../partials/Banner';
import {setCompany} from "../redux/actions/companyActions";
import CholesterolBarComponent from "../partials/Dashboard/component/CholesterolBar.component";
import HealthStatusPercentageComponent from "../partials/Dashboard/component/HealthStatusPercentage.component";
import UricAcidBarComponent from "../partials/Dashboard/component/UricAcidBar.component";
import BloodSugarBarComponent from "../partials/Dashboard/component/BloodSugarBar.component";
import HypertensionBarComponent from "../partials/Dashboard/component/HypertensionBar.component";

import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
import BodyMassIndexBarComponent from "../partials/Dashboard/component/BodyMassIndexBar.component";

import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css';

am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_kelly);

function Dashboard() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [allEmployeeData, setAllEmployeeData] = useState(null);
  const [allEmployeePercentage, setAllEmployeePercentage] = useState({});
  const [diagnoseTotal, setDiagnoseTotal] = useState({});
  const [finishGetAllData, setFinishGetAllData] = useState(false);

    useEffect(() => {
          getAllData();
      }, []);

  const getAllData = () => {
      axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
          .then(res => {
              const params = {
                  companyId: res.data.data.companyId
              }

              axios.get(`${process.env.REACT_APP_BASE_API}/employee`, {params})
                  .then(response => {
                      setAllEmployeeData(response.data.data)
                  })

              axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/percentage`, {
                  params
              })
                  .then(res => {
                      setAllEmployeePercentage(res.data.data)
                  })
                  .catch(err => {
                      console.log(err)
                  })

              axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/diagnose-total`, { params })
                  .then(res => {
                      let employee = res.data.data.employee;
                      let employeeKeys = Object.keys(employee);

                      let cholesterol = res.data.data.cholesterol;
                      let cholesterolKeys = Object.keys(cholesterol);

                      let bloodSugar = res.data.data.bloodSugar;
                      let bloodSugarKeys = Object.keys(bloodSugar);

                      let uricAcid = res.data.data.uricAcid;
                      let uricAcidKeys = Object.keys(uricAcid);

                      let hypertension = res.data.data.hypertension;
                      let hypertensionKeys = Object.keys(hypertension);

                      let bodyMassIndex = res.data.data.bodyMassIndex;
                      let bodyMassIndexKeys = Object.keys(bodyMassIndex);

                      let employeeArr = [];
                      let cholesterolArr = [];
                      let bloodSugarArr = [];
                      let uricAcidArr = [];
                      let hypertensionArr = [];
                      let bodyMassIndexArr = [];

                      employeeKeys.forEach((key, idx) => {
                          employeeArr[idx] = employee[key];
                      });

                      cholesterolKeys.forEach((key, idx) => {
                          cholesterolArr[idx] = cholesterol[key];
                      });

                      bloodSugarKeys.forEach((key, idx) => {
                          bloodSugarArr[idx] = bloodSugar[key];
                      });

                      uricAcidKeys.forEach((key, idx) => {
                          uricAcidArr[idx] = uricAcid[key];
                      });

                      hypertensionKeys.forEach((key, idx) => {
                          hypertensionArr[idx] = hypertension[key];
                      });

                      bodyMassIndexKeys.forEach((key, idx) => {
                          bodyMassIndexArr[idx] = bodyMassIndex[key];
                      });

                      let objectDiagnoseTotal = {
                          employee: employeeArr,
                          cholesterol: cholesterolArr,
                          bloodSugar: bloodSugarArr,
                          uricAcid: uricAcidArr,
                          hypertension: hypertensionArr,
                          bodyMassIndex: bodyMassIndexArr
                      }

                      setDiagnoseTotal(objectDiagnoseTotal);
                      setFinishGetAllData(true);
                  })
          })
  }

  if (finishGetAllData === false) {
      return (
          <div className="flex h-screen overflow-hidden bg-gray-800">
              <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
              <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                  <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                  <main>
                      <Skeleton count={3} />
                  </main>
                  <Banner />
              </div>
          </div>
      );
  } else {
      return <div className="flex h-screen overflow-hidden bg-gray-800">
          <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
          <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
              <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
              <main>
                  <div className="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
                      <div className="flex flex-col gap-6">
                          <div className="grid grid-cols-12 gap-6">
                              <HealthStatusPercentageComponent employeeData={allEmployeeData} allEmployeePercentage={allEmployeePercentage} />
                          </div>
                          <CholesterolBarComponent diagnoseTotal={diagnoseTotal} />
                          <HypertensionBarComponent diagnoseTotal={diagnoseTotal} />
                          <BloodSugarBarComponent diagnoseTotal={diagnoseTotal} />
                          <UricAcidBarComponent diagnoseTotal={diagnoseTotal} />
                          <BodyMassIndexBarComponent diagnoseTotal={diagnoseTotal} />
                      </div>
                  </div>
              </main>
              <Banner />
          </div>
      </div>
  }

}

const mapDispatchToProps = (dispatch) => ({
    setCompany: (payload) => dispatch(setCompany(payload))
});

export default connect(null, mapDispatchToProps)(Dashboard);