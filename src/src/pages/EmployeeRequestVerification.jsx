import React, {useEffect, useState} from 'react';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import {useHistory} from "react-router-dom";
import {Button} from "@mui/material";

export default function EmployeeRequestVerification() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [employees, setEmployees] = useState([]);
    const history = useHistory();

    useEffect(() => {
        getData()
    }, []);

    let theme = createTheme();
    theme = responsiveFontSizes(theme);

    const columns = [
        {
            name: "nik",
            label: "NIK",
            options: {
                filter: true,
                sort: false
            }
        },
        {
            name: "fullname",
            label: "Nama Lengkap",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "birthdate",
            label: "Tanggal Lahir",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "age",
            label: "Umur",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "division",
            label: "Divisi",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "approval",
            label: "Approval",
            options: {
                filter: false,
                sort: false,
            }
        }
    ];

    const row = employees.map((employee) => {
        return {
            nik: employee.nik,
            fullname: employee.fullname,
            birthdate: employee.birthdate,
            age: employee.age,
            division: employee.division,
            approval: <Button variant="contained" color="success" onClick={() => approvalRequest(employee.requestId, "approve")}>Approve</Button>
        }
    })

    const options = {
        filterType: 'checkbox',
        selectableRows: false
    };

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
            .then(res => {
                let params = {
                    companyId: res.data.data.companyId
                }

                axios.get(`${process.env.REACT_APP_BASE_API}/employee/verification/request/search/company`, {params})
                    .then(res => {
                        setEmployees(res.data.data)
                    })
            })

    }

    const approvalRequest = (requestId, approval) => {
        axios.post(`${process.env.REACT_APP_BASE_API}/employee/verification/approval`, {
            requestId: requestId,
            approvalStatus: approval
        })
            .then(res => {
                if(res.data.success === true) {
                    toast.success(
                        <div className='flex flex-col space-y-2'>
                            <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil approve request</h1>
                        </div>,
                        {
                            position: "bottom-right",
                            autoClose: 2000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                history.push("/employee/request-verification")
                            }
                        })
                }
            })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
            {/* Sidebar */}
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            {/* Content area */}
            <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                {/*  Site header */}
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <main>
                    <div className="px-4 sm:px-6 lg:px-8 py-8">
                        {/*<ThemeProvider theme={theme}>*/}
                        {/*    <MUIDataTable*/}
                        {/*        title={"Permintaan Verifikasi Karyawan Baru"}*/}
                        {/*        data={row}*/}
                        {/*        columns={columns}*/}
                        {/*        options={options}*/}
                        {/*    />*/}
                        {/*</ThemeProvider>*/}
                    </div>
                </main>
            </div>
            <ToastContainer />
        </div>
    )
}