import React, {useEffect, useState} from 'react';
import { ThemeProvider } from "@mui/styles";
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";

export default function EmployeeMedicalCheckup() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [medicalCheckups, setMedicalCheckups] = useState([]);

    useEffect(() => {
        getData();
    }, []);

    let theme = createTheme();
    theme = responsiveFontSizes(theme);

    const columns = [
    {
     name: "nik",
     label: "NIK",
     options: {
      filter: true,
      sort: false
     }
    },
    {
     name: "fullname",
     label: "Nama Lengkap",
     options: {
      filter: true,
      sort: false,
     }
    },
    {
     name: "birthdate",
     label: "Tanggal Lahir",
     options: {
      filter: true,
      sort: false,
     }
    },
    {
     name: "division",
     label: "Divisi",
     options: {
      filter: true,
      sort: false,
     }
    },
    {
        name: "height",
        label: "Tinggi Badan",
        options: {
            filter: true,
            sort: false,
        }
    },
    {
        name: "weight",
        label: "Berat Badan",
        options: {
            filter: true,
            sort: false,
        }
    },
    {
      name: "sistole",
      label: "Tekanan Darah",
      options: {
       filter: true,
       sort: false,
      }
     },
     {
      name: "blood_sugar_level",
      label: "Gula Darah",
      options: {
       filter: true,
       sort: false,
      }
     },
     {
      name: "cholesterol_level",
      label: "Kolesterol",
      options: {
       filter: true,
       sort: false,
      }
     },
     {
      name: "uric_acid_level",
      label: "Asam Urat",
      options: {
       filter: true,
       sort: false,
      }
     },
     {
      name: "checkup_date",
      label: "Tanggal Medical Checkup",
      options: {
       filter: true,
       sort: false,
      }
     },
    ];
   
    const options = {
    filterType: 'checkbox',
    selectableRows: false
    };

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
            .then(res => {
                let params = {
                    companyId: res.data.data.companyId
                }

                axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/master/company`, { params })
                    .then(res => {
                        setMedicalCheckups(res.data.data)
                    })
            })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
          {/* Sidebar */}
          <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
          {/* Content area */}
          <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
            {/*  Site header */}
            <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <main>
              <div className="px-4 sm:px-6 lg:px-8 py-8">
                  {/*<ThemeProvider theme={theme}>*/}
                  {/*    <MUIDataTable*/}
                  {/*        title={"Data Medical Checkup Karyawan"}*/}
                  {/*        data={medicalCheckups.map(medicalCheckup => {*/}
                  {/*            return [*/}
                  {/*                medicalCheckup.employee.nik,*/}
                  {/*                medicalCheckup.employee.fullname,*/}
                  {/*                medicalCheckup.employee.birthdate,*/}
                  {/*                medicalCheckup.employee.division,*/}
                  {/*                medicalCheckup.height,*/}
                  {/*                medicalCheckup.weight,*/}
                  {/*                medicalCheckup.sistole,*/}
                  {/*                medicalCheckup.blood_sugar_level,*/}
                  {/*                medicalCheckup.cholesterol_level,*/}
                  {/*                medicalCheckup.uric_acid_level,*/}
                  {/*                medicalCheckup.checkup_date*/}
                  {/*            ]*/}
                  {/*        })}*/}
                  {/*        columns={columns}*/}
                  {/*        options={options}*/}
                  {/*    />*/}
                  {/*</ThemeProvider>*/}
              </div>
            </main>
          </div>
        </div>
    )
}