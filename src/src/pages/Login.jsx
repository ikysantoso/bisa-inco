import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import axios from 'axios';

export default function Login() {
    const history = useHistory()
    const [inputAccess, setInputAccess] = useState({})
    const [passwordShown, setPasswordShown] = useState(false)

    const doSignIn = (e) => {
        e.preventDefault()

        if (Object.keys(inputAccess).length === 0 || (inputAccess.email === ('' || undefined)) || (inputAccess.password === ('' || undefined))) {
            toast.error(
                <h1 className='text-md' style={{fontWeight: 'bold'}}>Email atau Password tidak boleh kosong</h1>,
                {
                    position: "bottom-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: false,
                    draggable: false,
                    progress: undefined,
                })
        } else {
            axios.post(`${process.env.REACT_APP_BASE_API}/auth/login`, inputAccess)
                .then((res) => {
                    const sessionToken = res.data.data.sessionToken
                    const id = res.data.data.id

                    localStorage.setItem("sessionToken", sessionToken)
                    localStorage.setItem("id", id)

                    toast.success(
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Sign-in berhasil</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 1000,
                            hideProgressBar: true,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                history.push("/")
                            }
                        })
                })
                .catch((err) => {
                    toast.error(
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Email atau Password salah</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                        })
                })
        }
    }
    return (
    <>
        <div className="flex flex-col items-center justify-center h-screen space-y-8 bg-gray-800" style={{fontFamily: 'Roboto'}}>
            <div className="flex flex-row items-center justify-center mt-16 space-x-4">
                <h2 className="font-bold text-3xl text-white" style={{fontFamily: 'Georgia'}}>Inco</h2>
            </div>
            <form className="flex flex-col items-center justify-center space-y-6">
                <div className="relative mt-1">
                    <input type="text" className="h-10 p-4 text-sm font-light border border-gray-200 rounded w-60 focus:outline-none focus:border-blue-400" placeholder="Email" onChange={({target}) => setInputAccess(state => ({...state, email: target.value}))} value={inputAccess.email || ''} />
                </div>
                <div className="relative mt-1 mb-2">
                    <div className="flex flex-col items-end space-y-2">
                        <input type={passwordShown ? 'text' : 'password'} className="h-10 p-4 text-sm font-light border border-gray-200 rounded w-60 focus:outline-none focus:border-blue-400" placeholder="Password" onChange={({target}) => setInputAccess(state => ({...state, password: target.value}))} value={inputAccess.password || ''} />
                        <button type="button" onClick={(e) => setPasswordShown(!passwordShown)} className="text-sm text-white">
                            {passwordShown ? 'Hide Password' : 'Show Password'}
                        </button>
                    </div>
                </div>
                <button className="text-blue-500 border-2 border-blue-500 rounded-lg hover:bg-blue-500 hover:text-white w-60 h-9" onClick={doSignIn}>Sign in</button>
            </form>
        </div>
        <ToastContainer />
    </>
    )
}