import React, {useEffect, useState} from 'react';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";

export default function EmployeeList() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [employees, setEmployees] = useState([]);

    useEffect(() => {
        getData();
    }, []);


  let theme = createTheme();
  theme = responsiveFontSizes(theme);

  const columns = [
    {
     name: "nik",
     label: "NIK",
     options: {
      filter: true,
      sort: false
     }
    },
    {
     name: "fullname",
     label: "Nama Lengkap",
     options: {
      filter: true,
      sort: false,
     }
    },
    {
     name: "birthdate",
     label: "Tanggal Lahir",
     options: {
      filter: true,
      sort: false,
     }
    },
    {
     name: "division",
     label: "Divisi",
     options: {
      filter: true,
      sort: false,
     }
    },
   ];
   
  const options = {
    filterType: 'checkbox',
    selectableRows: false 
  };

  const getData = () => {
      axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
          .then(res => {
              let params = {
                  companyId: res.data.data.companyId
              }

              axios.get(`${process.env.REACT_APP_BASE_API}/employee/company`, { params })
                  .then(res => {
                      setEmployees(res.data.data);
                  });
          });
 };

  return (
    <div className="flex h-screen overflow-hidden bg-gray-800">
      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
      {/* Content area */}
      <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
        <main>
            <div className="px-4 sm:px-6 lg:px-8 py-8">
                {/*<ThemeProvider theme={theme}>*/}
                {/*    <MUIDataTable*/}
                {/*        title={"Data Karyawan"}*/}
                {/*        data={employees}*/}
                {/*        columns={columns}*/}
                {/*        options={options}*/}
                {/*    />*/}
                {/*</ThemeProvider>*/}
            </div>
        </main>
      </div>
    </div>
  )
}