import {ActionTypes} from "../constants/action-types";

const initialState = {
    company: {},
    user: {},
};

export const companyReducer = (state = initialState, { type, payload }) => {
    switch(type) {
        case ActionTypes.SET_COMPANY:
            return { ...state, company: payload };
        default:
            return state;
    }
};