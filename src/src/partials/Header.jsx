import React, {useEffect, useState} from 'react';
import UserMenu from './header/UserMenu';
import axios from "axios";
import {useHistory} from "react-router-dom";
import {ArrowBack} from "@material-ui/icons";

function Header({
  sidebarOpen,
  setSidebarOpen,
  useBack,
}) {
  const [userData, setUserData] = useState({});
  
  const history = useHistory();

  useEffect(() => {
    getUserDataById()
  }, []);

  const getUserDataById = () => {
    axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
        .then(res => {
          setUserData(res.data.data)
        })
  }

  return (
    <header className="sticky top-0 z-30 bg-transparent">
      <div className="px-4 sm:px-6 lg:px-8">
        <div className="flex items-center justify-between h-16 -mb-px">
          {
            useBack ?
            <div className="flex" onClick={() => history.goBack()}>
              <p className="font-bold text-white">Back</p>
            </div> : ""
          }
          <div className="flex">
            <button
              className="text-gray-500 hover:text-gray-600 lg:hidden"
              aria-controls="sidebar"
              aria-expanded={sidebarOpen}
              onClick={() => setSidebarOpen(!sidebarOpen)}
            >
              <span className="sr-only">Open sidebar</span>
              <svg className="w-6 h-6 fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <rect x="4" y="5" width="16" height="2" />
                <rect x="4" y="11" width="16" height="2" />
                <rect x="4" y="17" width="16" height="2" />
              </svg>
            </button>
          </div>
          <div className="flex items-center">
            <UserMenu userData={userData} />
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;