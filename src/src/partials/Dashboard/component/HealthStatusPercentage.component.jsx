import React from 'react';
import BarChart from '../../../charts/BarChart03';

import { tailwindConfig } from '../../../utils/Utils';

function HealthStatusPercentageComponent({ employeeData, allEmployeePercentage }) {
    if ((allEmployeePercentage.cholesterol) && (allEmployeePercentage.stageOneHypertension)
        && (allEmployeePercentage.stageTwoHypertension) && (allEmployeePercentage.bloodSugar)
        && (allEmployeePercentage.uricAcid)) {
        let chartData = {
            labels: ['Health Status'],
            employee: employeeData.length,
            datasets: [
                {
                    label: 'Kolesterol',
                    data: [Math.round(allEmployeePercentage.cholesterol.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Hipertensi Stage 1',
                    data: [Math.round(allEmployeePercentage.stageOneHypertension.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[800],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[900],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Hipertensi Stage 2',
                    data: [Math.round(allEmployeePercentage.stageTwoHypertension.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[900],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[1000],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Gula Darah',
                    data: [Math.round(allEmployeePercentage.bloodSugar.counter)],
                    backgroundColor: tailwindConfig().theme.colors.blue[500],
                    hoverBackgroundColor: tailwindConfig().theme.colors.blue[600],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Asam Urat',
                    data: [Math.round(allEmployeePercentage.uricAcid.counter)],
                    backgroundColor: tailwindConfig().theme.colors.blue[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.gray[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                }
            ],
        };

        return (
            <div className="col-span-full xl:col-span-6 bg-white rounded-lg">
                <header className="px-5 py-4">
                    <h2 className="font-semibold text-gray-800">Status Kesehatan Karyawan Saat Ini</h2>
                </header>
                <div className="px-5 py-3">
                    <div className="flex items-start">
                        <div className="text-3xl font-bold text-gray-800 mr-2">{employeeData.length} Karyawan</div>
                    </div>
                </div>
                <div className="grow">
                    <BarChart data={chartData} width={595} height={40}/>
                </div>
            </div>
        );
    }

    return (
        <div>
            <h1>Loading</h1>
        </div>
    )
}

export default HealthStatusPercentageComponent;
