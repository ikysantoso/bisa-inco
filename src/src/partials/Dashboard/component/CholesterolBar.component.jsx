import React, {useEffect, useRef} from 'react';
import BarChart from '../../../charts/BarChart01';

import { tailwindConfig } from '../../../utils/Utils';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

function CholesterolBarComponent({ diagnoseTotal }) {
    const chartRef = useRef(null);

    const CHART_ID = 'cholesterol_chart';

    // Add data
    let data = diagnoseTotal.cholesterol;

    useEffect(() => {
        if (!chartRef.current) {
            chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
            chartRef.current.colors.step = 2;

            chartRef.current.data = data;

            chartRef.current.legend = new am4charts.Legend()
            chartRef.current.legend.position = 'top'
            chartRef.current.legend.paddingBottom = 20
            chartRef.current.legend.labels.template.maxWidth = 95

            var xAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis())
            xAxis.dataFields.category = 'period'
            xAxis.renderer.cellStartLocation = 0.1
            xAxis.renderer.cellEndLocation = 0.9
            xAxis.renderer.grid.template.location = 0;

            var yAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
            yAxis.min = 0;

            function createSeries(value, name) {
                var series = chartRef.current.series.push(new am4charts.ColumnSeries())
                series.dataFields.valueY = value
                series.dataFields.categoryX = 'period'
                series.name = name

                series.events.on("hidden", arrangeColumns);
                series.events.on("shown", arrangeColumns);

                var bullet = series.bullets.push(new am4charts.LabelBullet())
                bullet.interactionsEnabled = false
                bullet.dy = 30;
                bullet.label.text = '{valueY}'
                bullet.label.fill = am4core.color('#ffffff')

                return series;
            }

            createSeries('normal', 'Normal');
            createSeries('cholesterol', 'Kolesterol');

            function arrangeColumns() {

                var series = chartRef.current.series.getIndex(0);

                var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                if (series.dataItems.length > 1) {
                    var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                    var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                    var delta = ((x1 - x0) / chartRef.current.series.length) * w;
                    if (am4core.isNumber(delta)) {
                        var middle = chartRef.current.series.length / 2;

                        var newIndex = 0;
                        chartRef.current.series.each(function (series) {
                            if (!series.isHidden && !series.isHiding) {
                                series.dummyData = newIndex;
                                newIndex++;
                            } else {
                                series.dummyData = chartRef.current.series.indexOf(series);
                            }
                        })
                        var visibleCount = newIndex;
                        var newMiddle = visibleCount / 2;

                        chartRef.current.series.each(function (series) {
                            var trueIndex = chartRef.current.series.indexOf(series);
                            var newIndex = series.dummyData;

                            var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                            series.animate({
                                property: "dx",
                                to: dx
                            }, series.interpolationDuration, series.interpolationEasing);
                            series.bulletsContainer.animate({
                                property: "dx",
                                to: dx
                            }, series.interpolationDuration, series.interpolationEasing);
                        })
                    }
                }
            }
        }

        return () => {
            chartRef.current && chartRef.current.dispose();
        };
    }, []);

    React.useEffect(() => {
        if (chartRef.current) {
            chartRef.current.data = data;
        }
    }, [data]);

    // Handle component unmounting, dispose chart
    React.useEffect(() => {
        return () => {
            chartRef.current && chartRef.current.dispose();
        };
    }, []);

    return (
        <div className="flex flex-col col-span-full sm:col-span-6 bg-white shadow-lg rounded-lg border border-gray-200">
            <header className="px-5 py-4">
                <h2 className="font-semibold text-gray-800">Grafik Data Kolesterol</h2>
            </header>
            <div
                id={CHART_ID}
                style={{
                    width: '100%',
                    height: '300px',
                    margin: '50px 0'
                }}
            />
        </div>
    );
}

export default CholesterolBarComponent;
