import React, { useEffect, useRef } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

function BodyMassIndexBarComponent({ diagnoseTotal }) {

    const chartRef = useRef(null);

    const data = diagnoseTotal.bodyMassIndex;

    useEffect(() => {
        if (!chartRef.current) {
            chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);

            chartRef.current.data = data;

            // Add X Axis
            let xAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis());
            xAxis.dataFields.category = "period";
            xAxis.renderer.grid.template.location = 0;
            xAxis.renderer.minGridDistance = 20;

            // Add Y Axis
            let yAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
            yAxis.title.text = "Total";
            yAxis.calculateTotals = true;
            yAxis.min = 0;
            yAxis.max = 100;
            yAxis.strictMinMax = true;
            yAxis.renderer.labels.template.adapter.add("text", function(text) {
                return text;
            });

            // Create series
            let underweightSeries = chartRef.current.series.push(new am4charts.ColumnSeries());
            underweightSeries.dataFields.valueY = "underweight";
            underweightSeries.dataFields.valueYShow = "totalPercent";
            underweightSeries.dataFields.categoryX = "period";
            underweightSeries.name = "Underweight";
            underweightSeries.tooltipText = "{name}: [bold]{valueY}[/]";
            underweightSeries.stacked = true;

            let normalSeries = chartRef.current.series.push(new am4charts.ColumnSeries());
            normalSeries.dataFields.valueY = "normal";
            normalSeries.dataFields.valueYShow = "totalPercent";
            normalSeries.dataFields.categoryX = "period";
            normalSeries.name = "Normal";
            normalSeries.tooltipText = "{name}: [bold]{valueY}[/]";
            normalSeries.stacked = true;

            let overweightSeries = chartRef.current.series.push(new am4charts.ColumnSeries());
            overweightSeries.dataFields.valueY = "overweight";
            overweightSeries.dataFields.valueYShow = "totalPercent";
            overweightSeries.dataFields.categoryX = "period";
            overweightSeries.name = "Overweight";
            overweightSeries.tooltipText = "{name}: [bold]{valueY}[/]";
            overweightSeries.stacked = true;

            let obesseSeries = chartRef.current.series.push(new am4charts.ColumnSeries());
            obesseSeries.dataFields.valueY = "obesse";
            obesseSeries.dataFields.valueYShow = "totalPercent";
            obesseSeries.dataFields.categoryX = "period";
            obesseSeries.name = "Obesse";
            obesseSeries.tooltipText = "{name}: [bold]{valueY}[/]";
            obesseSeries.stacked = true;

            // // Series tooltip
            // series.tooltipText = '{categoryX}: [bold]{valueY}[/]';
            // series.tooltip.pointerOrientation = 'down';
            // series.tooltip.dy = -5;
            // series.tooltip.background.filters.clear()
            // series.tooltip.getFillFromObject = false;
            // series.tooltip.background.fill = am4core.color('#2a2b2e');
            // series.tooltip.background.stroke = am4core.color('#2a2b2e');

            // Add cursor
            chartRef.current.cursor = new am4charts.XYCursor();

            chartRef.current.legend = new am4charts.Legend()

        }

        return () => {
            chartRef.current && chartRef.current.dispose();
        };
    }, []);

    const CHART_ID = 'bodyMassIndex_chart';

    React.useEffect(() => {
        if (chartRef.current) {
            chartRef.current.data = data;
        }
    }, [data]);

    // Handle component unmounting, dispose chart
    React.useEffect(() => {
        return () => {
            chartRef.current && chartRef.current.dispose();
        };
    }, []);

    return (
        <div className="flex flex-col col-span-full sm:col-span-6 bg-white rounded-lg">
            <header className="px-5 py-4">
                <h2 className="font-semibold text-gray-800">Grafik Data BMI</h2>
            </header>
            <div
                id={CHART_ID}
                style={{
                    width: '100%',
                    height: '300px',
                    margin: '50px 0'
                }}
            />
        </div>
    );
}

export default BodyMassIndexBarComponent;
