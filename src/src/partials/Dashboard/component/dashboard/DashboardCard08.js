import React from 'react';
import LineChart from '../../../../charts/LineChart02';

// Import utilities
import { tailwindConfig } from '../../../../utils/Utils';

function DashboardCard08() {

  const chartData = {
    labels: [
      '12-01-2020', '01-01-2021', '02-01-2021',
      '03-01-2021', '04-01-2021', '05-01-2021',
      '06-01-2021', '07-01-2021', '08-01-2021',
      '09-01-2021', '10-01-2021', '11-01-2021',
      '12-01-2021', '01-01-2022', '02-01-2022',
      '03-01-2022', '04-01-2022', '05-01-2022',
      '06-01-2022', '07-01-2022', '08-01-2022',
      '09-01-2022', '10-01-2022', '11-01-2022',
      '12-01-2022', '01-01-2023',
    ],
    datasets: [
      // Indigo line
      {
        label: 'Cholesterol',
        data: [
          73, 64, 73, 69, 104, 104, 164,
          164, 120, 120, 120, 148, 142, 104,
          122, 110, 104, 152, 166, 233, 268,
          252, 284, 284, 333, 323,
        ],
        borderColor: tailwindConfig().theme.colors.indigo[400],
        fill: false,
        borderWidth: 2,
        tension: 0,
        pointRadius: 0,
        pointHoverRadius: 3,
        pointBackgroundColor: tailwindConfig().theme.colors.indigo[400],
      },
      // High Blood Pressure
      {
        label: 'High Blood Pressure',
        data: [
          80, 66, 140, 68, 110, 104, 190,
          174, 130, 190, 150, 190, 155, 120,
          152, 150, 180, 190, 140, 250, 290,
          300, 290, 300, 350, 400,
        ],
        borderColor: tailwindConfig().theme.colors.indigo[500],
        fill: false,
        borderWidth: 2,
        tension: 0,
        pointRadius: 0,
        pointHoverRadius: 3,
        pointBackgroundColor: tailwindConfig().theme.colors.indigo[500],
      },
      // Blue line
      {
        label: 'Normal',
        data: [
          184, 86, 42, 378, 42, 243, 38,
          120, 0, 0, 42, 0, 84, 0,
          276, 0, 124, 42, 124, 88, 88,
          215, 156, 88, 124, 64,
        ],
        borderColor: tailwindConfig().theme.colors.blue[400],
        fill: false,
        borderWidth: 2,
        tension: 0,
        pointRadius: 0,
        pointHoverRadius: 3,
        pointBackgroundColor: tailwindConfig().theme.colors.blue[400],
      },
      // Green line
      {
        label: 'Diabetes',
        data: [
          10, 5, 3, 20, 30, 40, 50,
          9, 2, 1, 5, 10, 100, 90,
          30, 50, 114, 90, 80, 50, 18,
          118, 100, 12, 30, 28,
        ],
        borderColor: tailwindConfig().theme.colors.green[500],
        fill: false,
        borderWidth: 2,
        tension: 0,
        pointRadius: 0,
        pointHoverRadius: 3,
        pointBackgroundColor: tailwindConfig().theme.colors.green[500],
      },
    ],
  };

  return (
    <div className="flex flex-col col-span-full xl:col-span-8 sm:col-span-6 bg-white shadow-lg rounded-sm border border-gray-200">
      <header className="px-5 py-4 border-b border-gray-100 flex items-center">
        <h2 className="font-semibold text-gray-800">Health Graphic Overtime (all employee)</h2>
      </header>
      {/* Chart built with Chart.js 3 */}
      {/* Change the height attribute to adjust the chart height */}
      <LineChart data={chartData} width={595} height={248} />
    </div>
  );
}

export default DashboardCard08;
