export const employeeMedicalSample = [
    {
        id: 1,
        nik: 3171111111111,
        fullname: 'Dicky Santoso',
        birthdate: '2022-13-1',
        age: 19,
        division: 'Software Engineer',
        highBloodPressure: '1/1 mmHg',
        sugarBlood: '1 mg/dL',
        cholesterol: '1 mg/dL',
        gout: '1.0 mg/dL'
    },
    {
        id: 2,
        nik: 31722222222,
        fullname: 'Candra Afrian',
        birthdate: '2022-14-1',
        age: 29,
        division: 'Data Analyst',
        highBloodPressure: '1/1 mmHg',
        sugarBlood: '1 mg/dL',
        cholesterol: '1 mg/dL',
        gout: '1.0 mg/dL'
    },
];