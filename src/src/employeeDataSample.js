import {Button} from "@material-ui/core";

export const employeeDataSample = [
    {
        id: 1,
        nik: 3171111111111,
        fullname: 'Dicky Santoso',
        birthdate: '2022-13-1',
        age: 19,
        division: 'Software Engineer',
        approval: <Button variant="outlined">Approve</Button>
    },
    {
        id: 2,
        nik: 3172222222,
        fullname: 'Candra Afrian',
        birthdate: '2022-14-1',
        age: 29,
        division: 'Data Analyst',
        approval: <Button variant="outlined">Approve</Button>
    },
];