import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({...rest}) => {
    return (
        localStorage.getItem('id') ? <Redirect to='/' /> : <Route {...rest} />
    );
};

export default PublicRoute;