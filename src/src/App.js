import React, { useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  useLocation
} from 'react-router-dom';
import WebFont from 'webfontloader';

import './css/style.scss';

import './charts/ChartjsConfig';

// Import pages
import Dashboard from './pages/Dashboard';
import EmployeeList from './pages/EmployeeList';
import PartnershipList from './pages/PartnershipList';
import EmployeeMedicalCheckup from './pages/EmployeeMedicalCheckup';
import SettingAccount from './pages/SettingAccount';
import Login from "./pages/Login";
import PublicRoute from "./config/PublicRoute";
import PrivateRoute from "./config/PrivateRoute";
import EmployeeRequestVerification from "./pages/EmployeeRequestVerification";
import DetailHealthStatus from "./pages/DetailHealthStatus";
import DetailPartnership from "./pages/DetailPartnership";

function App() {

  const location = useLocation();

  useEffect(() => {
    document.querySelector('html').style.scrollBehavior = 'auto'
    window.scroll({ top: 0 })
    document.querySelector('html').style.scrollBehavior = ''
    WebFont.load({
      google: {
        families: ['Georgia', 'Roboto']
      }
    })
  }, [location.pathname]); // triggered on route change

  return (
    <Router>
      <div>
        <Switch>
          <PublicRoute path ="/login" component={Login} />
          <PrivateRoute exact path ="/" component={Dashboard} />
          <PrivateRoute path="/employee/request-verification" component={EmployeeRequestVerification} />
          <PrivateRoute path="/employee/list" component={EmployeeList} />
          <PrivateRoute path="/medical-checkup/list" component={EmployeeMedicalCheckup} />
          <PrivateRoute path="/settings/account" component={SettingAccount} />
          <PrivateRoute path="/detail-health/:status" component={DetailHealthStatus} />
          <PrivateRoute path="/partnership/list" component={PartnershipList} />
          <PrivateRoute path="/detail-partnership/:id" component={DetailPartnership} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
