import React, { useEffect, useState } from 'react';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import {DataGrid, GridToolbarContainer, GridToolbarExport, GridToolbarFilterButton} from "@mui/x-data-grid";
import Box from '@mui/material/Box';
import {Button, Typography } from "@mui/material";
import UploadFileService from "../services/UploadFileService";
// import { makeStyles } from '@mui/styles';

import Modal from '@mui/material/Modal';


function CustomToolbar() {
    return (
        <GridToolbarContainer>
            <GridToolbarExport />
            <GridToolbarFilterButton />
        </GridToolbarContainer>
    );
}

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


// const BorderLinearProgress = makeStyles(theme => ({
//     root: {
//         height: 15,
//         borderRadius: 5,
//     },
//     colorPrimary: {
//         backgroundColor: "#EEEEEE",
//     },
//     bar: {
//         borderRadius: 5,
//         backgroundColor: '#1a90ff',
//     },
// }))(LinearProgress);

export default function EmployeeMedicalCheckup() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [medicalCheckups, setMedicalCheckups] = useState([]);
    const [declinedImportedMedicalCheckups, setDeclinedImportedMedicalCheckups] = useState([]);
    const [importProgress, setImportProgress] = useState(0);
    const [selectedImportFile, setSelectedImportFile] = useState(undefined);
    const [currentImportFile, setCurrentImportFile] = useState(undefined);
    const [chosenNakes, setChosenNakes] = useState({});

    const [modalOpen, setModalOpen] = React.useState(false);
    const handleModalOpen = () => setModalOpen(true);
    const handleModalClose = () => setModalOpen(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
            .then(res => {
                let params = {
                    companyId: res.data.data.companyId
                }

                axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/master/company`, { params })
                    .then(res => {
                        setMedicalCheckups(res.data.data)
                    })
            })
    }

    function createData(id, fullName, birthdate, division, bmi, hypertension, blood_sugar_level,
                        cholesterol_level, uric_acid_level, dangerScoreDiagnose, healthStatus) {
        return { id, fullName, birthdate, division, bmi, hypertension, blood_sugar_level,
            cholesterol_level, uric_acid_level, dangerScoreDiagnose, healthStatus };
    }

    let rows = medicalCheckups.map((medicalCheckup) => {
        let by_nakes_mark = "";
        
        if (medicalCheckup.nursing_number !== null && medicalCheckup.nursing_number !== "") {
            by_nakes_mark = "*";
        }

        if (medicalCheckup.inputted_by_special_nakes !== null && medicalCheckup.inputted_by_special_nakes !== 0) {
            by_nakes_mark = "**";
        }

        return createData(
            medicalCheckup.id, medicalCheckup.employee.fullname, medicalCheckup.employee.birthdate,
            medicalCheckup.employee.division, medicalCheckup.bmi + " " + by_nakes_mark, medicalCheckup.hypertension + " " + by_nakes_mark,
            medicalCheckup.blood_sugar_level + " " + by_nakes_mark, medicalCheckup.cholesterol_level + " " + by_nakes_mark, medicalCheckup.uric_acid_level + " " + by_nakes_mark,
            medicalCheckup.dangerScoreDiagnose, medicalCheckup.healthStatus
        );
    })

    const columns = [
        { 
            field: 'id', 
            headerName: 'ID', 
            width: 70,
        },
        {
            field: 'fullName',
            headerName: 'Nama Lengkap',
            width: 150,
        },
        {
            field: 'birthdate',
            headerName: 'Tanggal Lahir',
            width: 150,
        },
        {
            field: 'division',
            headerName: 'Divisi',
            width: 150,
        },
        {
            field: 'bmi', headerName: 'BMI', width: 150,
            renderCell: (cellValues) => {
                if (cellValues.value.includes("*")) {
                    return (
                        <>
                            {/*<button onClick={() => showNakesSpecialByNursingNumber(cellValues.value)}>*/}
                            {/*    {cellValues.value}*/}
                            {/*</button>*/}
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                } else {
                    return (
                        <>
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                }
            }
        },
        {
            field: 'hypertension', headerName: 'Hipertensi', width: 200,
            renderCell: (cellValues) => {
                if (cellValues.value.includes("*")) {
                    return (
                        <>
                            {/*<button onClick={() => showNakesSpecialByNursingNumber(cellValues.value)}>*/}
                            {/*    {cellValues.value}*/}
                            {/*</button>*/}
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                } else {
                    return (
                        <>
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                }
            }
        },
        {
            field: 'blood_sugar_level', headerName: 'Gula Darah', width: 150,
            renderCell: (cellValues) => {
                if (cellValues.value.includes("*")) {
                    return (
                        <>
                            {/*<button onClick={() => showNakesSpecialByNursingNumber(cellValues.value)}>*/}
                            {/*    {cellValues.value}*/}
                            {/*</button>*/}
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                } else {
                    return (
                        <>
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                }
            }
        },
        {
            field: 'cholesterol_level', headerName: 'Kolesterol', width: 150,
            renderCell: (cellValues) => {
                if (cellValues.value.includes("*")) {
                    return (
                        <>
                            {/*<button onClick={() => showNakesSpecialByNursingNumber(cellValues.value)}>*/}
                            {/*    {cellValues.value}*/}
                            {/*</button>*/}
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                } else {
                    return (
                        <>
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                }
            }
        },
        {
            field: 'uric_acid_level', headerName: 'Asam Urat', width: 150,
            renderCell: (cellValues) => {
                if (cellValues.value.includes("*")) {
                    return (
                        <>
                            {/*<button onClick={() => showNakesSpecialByNursingNumber(cellValues.value)}>*/}
                            {/*    {cellValues.value}*/}
                            {/*</button>*/}
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                } else {
                    return (
                        <>
                            <span>
                               {cellValues.value}
                            </span>
                        </>
                    );
                }
            }
        },
        {
            field: 'dangerScoreDiagnose',
            headerName: 'Indikator Bahaya Kesehatan',
            width: 200,
        },
        {
            field: 'healthStatus',
            headerName: 'Status Kebugaran',
            width: 200,
        },
    ];

    const selectFile = (event) => {
        setSelectedImportFile(event.target.files)
    }

    const uploadFile = () => {
        let currentImportFile = selectedImportFile[0];

        setCurrentImportFile(currentImportFile);

        UploadFileService.upload(currentImportFile, (event) => {
            setImportProgress(Math.round((100 * event.loaded) / event.total));
        })
            .then((response) => {
                window.location.reload();
            })
            .then((file) => {
            })
            .catch(() => {
                console.log("error import");
            })

        setSelectedImportFile(undefined);
    }

    const showNakesSpecialByNursingNumber = (id) => {
        axios.get(`${process.env.REACT_APP_BASE_API}/nakes-special/${id}`)
            .then(res => {
                setChosenNakes(res.data);

                handleModalOpen();
            })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
          {/* Sidebar */}
          <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
          {/* Content area */}
          <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
            {/*  Site header */}
            <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <main>
              <div className="px-4 sm:px-6 lg:px-8 py-8">
                  {/*<div className="flex flex-col space-y-2 mb-8">*/}
                  {/*    <span className="text-md text-white">*/}
                  {/*        NOTE:*/}
                  {/*    </span>*/}

                  {/*    <div className="flex flex-col space-y-6">*/}
                  {/*        <span className="text-md text-white">1. Tanda "Bintang 1 (*)" berarti data Medical Check Up dibuat oleh Tenaga Kesehatan yang terdaftar pada <b>BISA nakes.</b></span>*/}
                  {/*        <span className="text-md text-white">2. Tanda "Bintang 2 (**)" berarti data Medical Check Up dibuat oleh Tenaga Kesehatan yang terdaftar pada <b>Perusahaan.</b></span>*/}
                  {/*        <span className="text-md text-white">3. Untuk melihat keterangan lengkapnya, silahkan klik Tanda "Bintang(*) (**)" tersebut.</span>*/}
                  {/*    </div>*/}
                  {/*</div>*/}
                  {/*<div className="flex flex-row space-x-4 mb-8">*/}
                  {/*    <Button variant="contained" onClick={() => adjustMedicalCheckupData()}>*/}
                  {/*        <span className="text-white">Tampilkan Semua Data</span>*/}
                  {/*    </Button>*/}
                  {/*    <Button variant="contained" color="success" onClick={() => adjustMedicalCheckupData()}>*/}
                  {/*        <span className="text-white">Tampilkan Khusus * dan **</span>*/}
                  {/*    </Button>*/}
                  {/*</div>*/}
                  <Box sx={{ height: 400, width: '100%', backgroundColor: '#ffffff' }}>
                      <DataGrid
                          rows={rows}
                          columns={columns}
                          components={{
                              Toolbar: CustomToolbar,
                          }}
                      />
                  </Box>
              </div>

                <div className="px-4 sm:px-6 lg:px-8 py-8">
                    {currentImportFile && (
                        <Box className="mb25" display="flex" alignItems="center">
                            {/*<Box width="100%" mr={1}>*/}
                            {/*    <BorderLinearProgress variant="determinate" value={importProgress} />*/}
                            {/*</Box>*/}
                            <Box minWidth={35}>
                                <Typography variant="body2" color="textSecondary">{`${importProgress}%`}</Typography>
                            </Box>
                        </Box>)
                    }

                    {/*<label htmlFor="btn-upload">*/}
                    {/*    <input*/}
                    {/*        id="btn-upload"*/}
                    {/*        name="btn-upload"*/}
                    {/*        style={{ display: 'none' }}*/}
                    {/*        type="file"*/}
                    {/*        onChange={selectFile} />*/}
                    {/*    <Button*/}
                    {/*        className="btn-choose"*/}
                    {/*        variant="outlined"*/}
                    {/*        component="span" >*/}
                    {/*        Choose Files*/}
                    {/*    </Button>*/}
                    {/*</label>*/}
                    {/*<div className="file-name">*/}
                    {/*    {selectedImportFile && selectedImportFile.length > 0 ? selectedImportFile[0].name : null}*/}
                    {/*</div>*/}
                    {/*<Button*/}
                    {/*    className="btn-upload"*/}
                    {/*    color="primary"*/}
                    {/*    variant="contained"*/}
                    {/*    component="span"*/}
                    {/*    disabled={!selectedImportFile}*/}
                    {/*    onClick={uploadFile}>*/}
                    {/*    Upload*/}
                    {/*</Button>*/}
                </div>
            </main>
          </div>

            {/*<Modal*/}
            {/*    open={modalOpen}*/}
            {/*    onClose={handleModalClose}*/}
            {/*    aria-labelledby="modal-modal-title"*/}
            {/*    aria-describedby="modal-modal-description"*/}
            {/*>*/}
            {/*    <Box sx={modalStyle}>*/}
            {/*        <Typography id="modal-modal-fullname" variant="h6" component="h2">*/}
            {/*            {chosenNakes.fullname}*/}
            {/*        </Typography>*/}
            {/*        /!*<Typography id="modal-modal-instance-name" sx={{ mt: 1 }}>*!/*/}
            {/*        /!*    {chosenNakes.instance.name}*!/*/}
            {/*        /!*</Typography>*!/*/}
            {/*        <Typography id="modal-modal-instance-address" sx={{ mt: 1 }}>*/}
            {/*            {chosenNakes.instance}*/}
            {/*        </Typography>*/}
            {/*        <Typography id="modal-modal-nursing_number" sx={{ mt: 1 }}>*/}
            {/*            {chosenNakes.nursing_number}*/}
            {/*        </Typography>*/}
            {/*        <Button*/}
            {/*            className="btn-upload"*/}
            {/*            color="primary"*/}
            {/*            variant="contained"*/}
            {/*            component="span"*/}
            {/*            onClick={() => handleModalClose()}>*/}
            {/*            Close*/}
            {/*        </Button>*/}
            {/*    </Box>*/}
            {/*</Modal>*/}
        </div>
    )
}