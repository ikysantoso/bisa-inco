import React, {useEffect, useState} from 'react';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import {Button, Modal, TextField} from "@material-ui/core";
import Box from "@mui/material/Box";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

export default function CompanyList() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [companies, setcompanies] = useState([]);
    const [inputCompany, setInputCompany] = useState({});
    const [isModalOpen, setIsModalOpen] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    function createData(companyName, action, removeCompany) {
      return { companyName, action, removeCompany };
    }

    const rows = companies.map((company) => {
        return createData(company.companyName,
        <div style={{ display: "flex", justifyContent: "flex-start", gap: "10px" }}>
            <div className="">
                <Button variant="outlined"  onClick={(e) => {
                    e.preventDefault(); window.location.href=`/detail-company/${company.id}`;}}>Detail
                </Button>
            </div>
            <div className={`${company.companyName === "Bersama Indonesia Sehat Alami" ? 'hidden' : ''}`}>
                <Button variant="contained" color="error" onClick={() => removeCompany(company.id)}>Delete</Button>
            </div>
        </div>
        )
    });

    const removeCompany = (companyId) => {
        axios.delete(`${process.env.REACT_APP_BASE_API}/company/${companyId}`)
            .then(res => {
                if(res.data.success === true) {
                    toast.error(
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil Menghapus Data Company</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                window.location.reload();
                            }
                        }
                    )
                }
            }
        )
    }

    const getData = () => {
      axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
          .then(res => {
              let params = {
                  companyId: res.data.data.companyId
              }

              axios.get(`${process.env.REACT_APP_BASE_API}/company/company`, { params })
                  .then(res => {
                    setcompanies(res.data.data);
                  });
          });
    };

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        borderRadius: '10px',
        boxShadow: 24,
        p: 4,
    };

    const handleSave = () => {
        axios.post(`${process.env.REACT_APP_BASE_API}/company`, inputCompany)
        .then((res) => {
            if(res.data.success === true) {
                toast.success(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil Menambah Data Company</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 1000,
                        hideProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                        onClose: () => {
                            window.location.reload();
                        }
                    })
            }
        })
        .catch((err) => {
            toast.error(
                <h1 className='text-md' style={{fontWeight: 'bold'}}>Proses gagal dilakukan</h1>,
                {
                    position: "bottom-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: false,
                    draggable: false,
                    progress: undefined,
                })
        })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
          {/* Sidebar */}
          <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
          {/* Content area */}
          <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
            {/*  Site header */}
            <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <main>
                <div className="py-8">
                    <div className="px-4 sm:px-6 lg:px-8 py-6">
                        <Button variant="contained" onClick={() => setIsModalOpen(!isModalOpen)}>Tambah Perusahaan</Button>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Company Name</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.length > 0 ? rows.map((row) => (
                                        <TableRow key={row.number}>
                                            <TableCell component="th" scope="row">
                                                {row.companyName}
                                            </TableCell>                                
                                            <TableCell>{row.action} {row.removeCompany}</TableCell>
                                        </TableRow>
                                    )) :
                                    <div>
                                        <span>No Data</span>
                                    </div>}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
                <Modal
                    open={isModalOpen}
                    onClose={() => setIsModalOpen(!isModalOpen)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={modalStyle}>
                        <div className="flex flex-col gap-6">
                            <TextField
                                sx={{ my: 2 }}
                                id="outlined-basic"
                                label="Nama Perusahaan"
                                variant="outlined"
                                value={inputCompany.companyName}
                                onChange={({target}) => setInputCompany(state => ({...state, companyName: target.value}))}
                            />
                            <Button variant="outlined" onClick={handleSave}>Save</Button>
                        </div>
                    </Box>
                </Modal>
            </main>
          </div>
        <ToastContainer />
    </div>
    )
}