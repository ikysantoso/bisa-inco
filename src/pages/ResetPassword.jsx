import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {Button, TextField} from "@material-ui/core";
import {ToastContainer, toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function ResetPassword() {
    const [finishValidate, setFinishValidate] = useState(false);
    const [forbiddenAccess, setForbiddenAccess] = useState(false);
    const [password, setPassword] = useState("");

    useEffect(() => {
        validateAccess()
    }, []);

    const validateAccess = () => {
        let inputAccessValidation = {
            unique_access: window.location.pathname.split("/").pop()
        }

        axios.post(`${process.env.REACT_APP_BASE_API}/auth/reset-password/validate-unique-access`, inputAccessValidation)
            .then((res) => {
                setFinishValidate(true);
            })
            .catch((err) => {
                setForbiddenAccess(true);
                setFinishValidate(true);

                window.location.href = "/";
            })
    }

    const doChangePassword = () => {
        if (password === "") {
            toast.error(
                <h1 className='text-md' style={{fontWeight: 'bold'}}>Password tidak boleh kosong</h1>,
                {
                    position: "bottom-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: false,
                    draggable: false,
                    progress: undefined,
                })
        } else {
            let inputResetPassword = {
                unique_access:  window.location.pathname.split("/").pop(),
                new_password: password
            }

            axios.post(`${process.env.REACT_APP_BASE_API}/auth/reset-password/store`, inputResetPassword)
                .then((res) => {
                    window.location.href = "/";
                })
        }
    }

    if (finishValidate === true) {
        if (forbiddenAccess === true) {
            return (
                <div>
                    <span>
                        forbidden
                    </span>
                </div>
            );
        } else {
            return (
                <div className="flex flex-col space-y-6 justify-center items-center center-0 mt-8">
                    <TextField
                        sx={{ my: 2 }}
                        id="outlined-basic"
                        label="Password Baru"
                        variant="outlined"
                        onChange={({target}) => setPassword(target.value.trim())}
                    />

                    <Button variant="outlined" onClick={() => doChangePassword()}>Simpan</Button>

                    <ToastContainer />
                </div>
            );
        }
    } else {
        return (
            <div>
                loading
            </div>
        );
    }
}