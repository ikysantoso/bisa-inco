import React, {useEffect, useState} from 'react';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import {useParams} from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";

export default function DetailHealthStatus() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [employee, setEmployee] = useState([]);

    let { status } = useParams();

    useEffect( () => {
        getData();
    }, []);

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
            .then(res => {
                let healthStatus;

                switch(status) {
                    case "Hiperkolesterol":
                        healthStatus = 'cholesterol';
                        break;

                    case "Hipertensi Stage 1":
                        healthStatus = 'stageOneHypertension';
                        break;

                    case "Hipertensi Stage 2":
                        healthStatus = 'stageTwoHypertension';
                        break;

                    case "Asam Urat Tinggi":
                        healthStatus = 'uricAcid';
                        break;

                    case "Gula Darah Tinggi":
                        healthStatus = 'bloodSugar';
                        break;

                    case "Underweight":
                        healthStatus = 'underweight';
                        break;

                    case "Overweight":
                        healthStatus = 'overweight';
                        break;

                    case "Obese":
                        healthStatus = 'obese';
                        break;

                    case "Tidak Bugar":
                        healthStatus = 'notHealthy';
                        break;

                    case "Karyawan yang benar-benar (sehat dan tidak bugar)":
                        healthStatus = 'zeroDangerScoreDiagnose';
                        break;

                    case "Karyawan yang benar-benar (sehat dan bugar)":
                        healthStatus = 'healthy';
                        break;

                    case "Indikator Bahaya Kesehatan 1":
                        healthStatus = 'oneDangerScoreDiagnose';
                        break;

                    case "Indikator Bahaya Kesehatan 2":
                        healthStatus = 'twoDangerScoreDiagnose';
                        break;

                    case "Indikator Bahaya Kesehatan 3":
                        healthStatus = 'threeDangerScoreDiagnose';
                        break;

                    case "Indikator Bahaya Kesehatan 4":
                        healthStatus = 'fourDangerScoreDiagnose';
                        break;

                    case "Indikator Bahaya Kesehatan 5":
                        healthStatus = 'fiveDangerScoreDiagnose';
                        break;

                    case "Indikator Bahaya kesehatan lebih dari atau sama dengan 3":
                        healthStatus = 'dangerScoreDiagnose';
                        break;

                    default:
                        break;
                }

                const params = {
                    companyId: res.data.data.companyId,
                    diagnose: healthStatus
                }

                axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/specific-diagnose`, { params })
                    .then(res => {
                        setEmployee(res.data.data.employee);
                    })
            })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
            {/* Sidebar */}
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            {/* Content area */}
            <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                {/*  Site header */}
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} useBack={true} />
                <main>
                    <div className="col-span-full xl:col-span-6 rounded-lg">
                        <div className="p-3">
                            <TableContainer component={Paper}>
                                <Table aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="left">Nama Lengkap</TableCell>
                                            <TableCell align="left">Jenis Kelamin</TableCell>
                                            <TableCell align="left">Divisi</TableCell>
                                            <TableCell align="left">Umur</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            employee.map((data) => (
                                                    <TableRow key={data.employee_id}>
                                                        <TableCell component="th" scope="row">{data.fullname}</TableCell>
                                                        <TableCell align="left">{data.gender}</TableCell>
                                                        <TableCell align="left">{data.division}</TableCell>
                                                        <TableCell align="left">
                                                            {
                                                                Math.abs(
                                                                    new Date(Date.now() - new Date(data.birthdate).getTime()).getUTCFullYear() - 1970
                                                                )
                                                            }
                                                        </TableCell>
                                                    </TableRow>
                                            ))
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    )
}