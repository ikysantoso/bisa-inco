import React, {useEffect, useState} from 'react';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import {useHistory} from "react-router-dom";
import {Button} from "@mui/material";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";


export default function EmployeeRequestDelete() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [employees, setEmployees] = useState([]);
    const history = useHistory();

    useEffect(() => {
        getData()
    }, []);

    function createData(nik, fullName, birthdate, age, division, approval) {
        return { nik, fullName, birthdate, age, division, approval };
    }

    const rows = employees.map((employee) => {
        return createData(employee.nik, employee.fullname, employee.birthdate,
            employee.age, employee.division, <Button variant="outlined" onClick={() => deleteEmployeeRequest(employee.id)}>Approve</Button>)
    })

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
            .then(res => {
                let params = {
                    companyId: res.data.data.companyId
                }

                axios.get(`${process.env.REACT_APP_BASE_API}/employee/delete/request/search/company`, {params})
                    .then(res => {
                        setEmployees(res.data.data)
                    })
            })

    }

    const deleteEmployeeRequest = (id) => {
        axios.delete(`${process.env.REACT_APP_BASE_API}/employee/delete/request/${id}`, {
            id: id,
        })
            .then(res => {
                if(res.data.success === true) {
                    toast.success(
                        <div className='flex flex-col space-y-2'>
                            <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil approve request delete</h1>
                        </div>,
                        {
                            position: "bottom-right",
                            autoClose: 2000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                window.location.reload();
                            }
                        }
                    )
                }
            }
        )
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
            {/* Sidebar */}
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            {/* Content area */}
            <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                {/*  Site header */}
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <main>
                    <div className="px-4 sm:px-6 lg:px-8 py-8">
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>NIK</TableCell>
                                        <TableCell>Full Name</TableCell>
                                        <TableCell>Division</TableCell>
                                        <TableCell>Age</TableCell>
                                        <TableCell align="right">Approval</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.length > 0 ? rows.map((row) => (
                                        <TableRow key={row.number}>
                                            <TableCell component="th" scope="row">
                                                {row.nik}
                                            </TableCell>
                                            <TableCell>{row.fullName}</TableCell>
                                            <TableCell>{row.division}</TableCell>
                                            <TableCell>
                                                {
                                                    Math.abs(
                                                        new Date(Date.now() - new Date(row.birthdate).getTime()).getUTCFullYear() - 1970
                                                    )
                                                }
                                            </TableCell>
                                            <TableCell align="right">{row.approval}</TableCell>
                                        </TableRow>
                                    )) :
                                    <div>
                                        <span>No Data</span>
                                    </div>}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </main>
            </div>
            <ToastContainer />
        </div>
    )
}