import React, {useEffect, useState} from 'react';
import MUIDataTable from "mui-datatables";
import { ThemeProvider } from "@mui/styles";
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import {useParams} from "react-router-dom";
import Box from "@mui/material/Box";
import {DataGrid, GridToolbarContainer, GridToolbarExport} from "@mui/x-data-grid";
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import {Button, Modal, TextField} from "@material-ui/core";

function CustomToolbar() {
    return (
        <GridToolbarContainer>
            <GridToolbarExport />
        </GridToolbarContainer>
    );
}


export default function DetailCompany() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [users, setUsers] = useState([]);
  const [medicalCheckups, setMedicalCheckups] = useState([]);
  const [inputUser, setInputUser] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false);

  let { id } = useParams();

  useEffect(() => {
    getData();
  }, []);


  let theme = createTheme();
  theme = responsiveFontSizes(theme);

  const columns = [
    {
     name: "name",
     label: "Nama",
     options: {
      filter: true,
      sort: false
     }
    },
    {
      name: "email",
      label: "Email",
      options: {
          filter: true,
          sort: false,
      }
    },
    {
     name: "role",
     label: "Role",
     options: {
      filter: true,
      sort: false,
     }
    },
    {
        name: "action",
        label: "Action",
        options: {
         filter: true,
         sort: false,
        }
       },
   ];

   const row = users.map((user) => {
    return {
        name: user.name,
        email: user.email,
        role: user.role,
        action: <Button variant="outlined" onClick={() => removeUsers(user.id)}>Delete</Button>
       }
    })
    
    const removeUsers = (userId) => {
        axios.delete(`${process.env.REACT_APP_BASE_API}/user/${userId}`)
            .then(res => {
                if(res.data.success === true) {
                    toast.error(
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil Menghapus Data User</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                window.location.reload();
                            }
                        }
                    )
                }
            }
        )
    }

    const columnsMedicalCheckups = [
        { field: 'id', headerName: 'ID', width: 70 },
        {
            field: 'fullName',
            headerName: 'Nama Lengkap',
            width: 150,
        },
        {
            field: 'birthdate',
            headerName: 'Tanggal Lahir',
            width: 150,
        },
        {
            field: 'division',
            headerName: 'Divisi',
            width: 150,
        },
        {
            field: 'bmi',
            headerName: 'BMI',
            width: 150,
        },
        {
            field: 'hypertension',
            headerName: 'Hipertensi',
            width: 200,
        },
        {
            field: 'blood_sugar_level',
            headerName: 'Gula Darah',
            width: 150,
        },
        {
            field: 'cholesterol_level',
            headerName: 'Kolesterol',
            width: 150,
        },
        {
            field: 'uric_acid_level',
            headerName: 'Asam Urat',
            width: 150,
        },
        {
            field: 'dangerScoreDiagnose',
            headerName: 'Indikator Bahaya Kesehatan',
            width: 200,
        },
        {
            field: 'healthStatus',
            headerName: 'Status Kebugaran',
            width: 200,
        },
    ];
   
  const options = {
    filterType: 'checkbox',
    selectableRows: false 
  };

  const getData = () => {
      axios.get(`${process.env.REACT_APP_BASE_API}/user/company/search/${id}`)
          .then(res => {
              setUsers(res.data.data);
          });

      let params = {
          companyId: id
      }

      axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/master/company`, { params })
          .then(res => {
              setMedicalCheckups(res.data.data);
          })
 };

    function createData(id, fullName, birthdate, division, bmi, hypertension, blood_sugar_level,
                        cholesterol_level, uric_acid_level, dangerScoreDiagnose, healthStatus) {
        return { id, fullName, birthdate, division, bmi, hypertension, blood_sugar_level,
            cholesterol_level, uric_acid_level, dangerScoreDiagnose, healthStatus };
    }

    const rows = medicalCheckups.map((medicalCheckup) => {
        return createData(
            medicalCheckup.id, medicalCheckup.employee.fullname, medicalCheckup.employee.birthdate,
            medicalCheckup.employee.division, medicalCheckup.bmi, medicalCheckup.hypertension,
            medicalCheckup.blood_sugar_level, medicalCheckup.cholesterol_level, medicalCheckup.uric_acid_level,
            medicalCheckup.dangerScoreDiagnose, medicalCheckup.healthStatus
        )
    });

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        borderRadius: '10px',
        boxShadow: 24,
        p: 4,
    };

    const handleSave = () => {
        inputUser.companyId = id;

        axios.post(`${process.env.REACT_APP_BASE_API}/auth/register`, inputUser)
        .then((res) => {
            if(res.data.success === true) {
                toast.success(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil Menambah Data User</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 1000,
                        hideProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                        onClose: () => {
                            window.location.reload();
                        }
                    })
            }
        })
        .catch((err) => {
            toast.error(
                <h1 className='text-md' style={{fontWeight: 'bold'}}>Proses gagal dilakukan</h1>,
                {
                    position: "bottom-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: false,
                    draggable: false,
                    progress: undefined,
                })
        })
    }

  return (
    <div className="flex h-screen overflow-hidden bg-gray-800">
      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
      {/* Content area */}
      <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
        <main>
          <div className="py-8">
              <div className="px-4 sm:px-6 lg:px-8 py-6">
                  <Button variant="contained" onClick={() => setIsModalOpen(!isModalOpen)}>Registrasi User</Button>
              </div>
            <div className="px-4 sm:px-6 lg:px-8 py-5">
                <ThemeProvider theme={theme}>
                  <MUIDataTable
                    title={"Data User"}
                    data={row}
                    columns={columns}
                    options={options}
                  />
                </ThemeProvider>
            </div>
              <div className="px-4 sm:px-6 lg:px-8 py-8">
                  <label className="font-bold" style={{color: 'white'}}>
                      Data Medical Checkup
                  </label>
                  <Box sx={{ height: 400, width: '100%', backgroundColor: '#ffffff' }}>
                      <DataGrid
                          rows={rows}
                          columns={columnsMedicalCheckups}
                          components={{
                              Toolbar: CustomToolbar,
                          }}
                      />
                  </Box>
              </div>
          </div>
            <Modal
                open={isModalOpen}
                onClose={() => setIsModalOpen(!isModalOpen)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={modalStyle}>
                    <div className="flex flex-col gap-6">
                        <TextField
                            sx={{ my: 2 }}
                            id="outlined-basic"
                            label="Nama Lengkap"
                            variant="outlined"
                            value={inputUser.name}
                            onChange={({target}) => setInputUser(state => ({...state, name: target.value}))}
                        />
                        <TextField
                            sx={{ my: 2 }}
                            id="outlined-basic"
                            label="Email"
                            variant="outlined"
                            value={inputUser.email}
                            onChange={({target}) => setInputUser(state => ({...state, email: target.value}))}
                        />
                        <TextField
                            sx={{ my: 2 }}
                            id="outlined-basic"
                            label="Jabatan"
                            variant="outlined"
                            value={inputUser.role}
                            onChange={({target}) => setInputUser(state => ({...state, role: target.value}))}
                        />
                        <Button variant="outlined" onClick={handleSave}>Save</Button>
                    </div>
                </Box>
            </Modal>
        </main>
      </div>
    <ToastContainer />
    </div>
  )
}