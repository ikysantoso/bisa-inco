import React, { useEffect, useState } from 'react';
import { ThemeProvider } from "@mui/styles";
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import { useParams } from "react-router-dom";
import { Box, Button, Modal, TextField } from "@material-ui/core";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

export default function DetailPartnership() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [partner, setPartner] = useState({});
    const [users, setUsers] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [inputUser, setInputUser] = useState({});

    let { id } = useParams();

    useEffect(() => {
        getData();
    }, []);

    let theme = createTheme();
    theme = responsiveFontSizes(theme);

    const columns = [
        {
            name: "name",
            label: "Nama Lengkap",
            options: {
                filter: true,
                sort: false
            }
        },
        {
            name: "email",
            label: "Email",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "role",
            label: "Jabatan",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "access_status",
            label: "Akses Status",
            options: {
                filter: false,
                sort: false,
            }
        },
    ];

    const options = {
        filterType: 'checkbox',
        selectableRows: false
    };

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/partnership/${id}`)
            .then(res => {
                setPartner(res.data.data)
                setUsers(res.data.data.userData)
            })
    }

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        borderRadius: '10px',
        boxShadow: 24,
        p: 4,
    };

    const handleSave = () => {
        inputUser.companyId = id;

        axios.post(`${process.env.REACT_APP_BASE_API}/auth/register`, inputUser)
            .then((res) => {
                toast.success(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil menambah data user</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 1000,
                        hideProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                        onClose: () => {
                            window.location.reload();
                        }
                    })
            })
            .catch((err) => {
                toast.error(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>{err.response.data.message}</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                    })
            })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
            {/* Sidebar */}
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            {/* Content area */}
            <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                {/*  Site header */}
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} useBack={true} />
                <main>
                    <div className="col-span-full xl:col-span-6 rounded-lg">
                        <div className="flex flex-col gap-6 p-3">
                            <div className="flex flex-col gap-6">
                                <div className="flex flex-col">
                                    <p className="text-sm text-white">Company ID</p>
                                    <p className="font-bold text-white">{partner.companyId}</p>
                                </div>
                                <div className="flex flex-col">
                                    <p className="text-sm text-white">Company Name</p>
                                    <p className="font-bold text-white">{partner.companyName}</p>
                                </div>
                            </div>
                            <div className="mt-2">
                                <Button variant="contained" color="success" onClick={() => setIsModalOpen(!isModalOpen)}>
                                    <p className="font-bold">
                                        Add New User
                                    </p>
                                </Button>
                            </div>
                            {/*<ThemeProvider theme={theme}>*/}
                            {/*    <MUIDataTable*/}
                            {/*        title={`Data User`}*/}
                            {/*        data={users}*/}
                            {/*        columns={columns}*/}
                            {/*        options={options}*/}
                            {/*    />*/}
                            {/*</ThemeProvider>*/}
                        </div>
                    </div>
                    <Modal
                        open={isModalOpen}
                        onClose={() => setIsModalOpen(!isModalOpen)}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={modalStyle}>
                            <div className="flex flex-col gap-6">
                                <TextField
                                    sx={{ my: 2 }}
                                    id="outlined-basic"
                                    label="Nama Lengkap"
                                    variant="outlined"
                                    value={inputUser.name}
                                    onChange={({target}) => setInputUser(state => ({...state, name: target.value}))}
                                />
                                <TextField
                                    sx={{ my: 2 }}
                                    id="outlined-basic"
                                    label="Email"
                                    variant="outlined"
                                    value={inputUser.email}
                                    onChange={({target}) => setInputUser(state => ({...state, email: target.value}))}
                                />
                                <TextField
                                    sx={{ my: 2 }}
                                    id="outlined-basic"
                                    label="Jabatan"
                                    variant="outlined"
                                    value={inputUser.role}
                                    onChange={({target}) => setInputUser(state => ({...state, role: target.value}))}
                                />
                                <Button variant="outlined" onClick={handleSave}>Save</Button>
                            </div>
                        </Box>
                    </Modal>
                </main>
            </div>
            <ToastContainer />
        </div>
    )
}