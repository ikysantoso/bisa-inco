import React, {useEffect, useState} from 'react';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import {Button} from "@mui/material";

export default function EmployeeList() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [employees, setEmployees] = useState([]);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
      axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
          .then(res => {
              let params = {
                  companyId: res.data.data.companyId
              }

              axios.get(`${process.env.REACT_APP_BASE_API}/employee/company`, { params })
                  .then(res => {
                      setEmployees(res.data.data);
                  });
          });
    };

    function createData(nik, fullName, birthdate, age, division, removalAccount) {
        return { nik, fullName, birthdate, age, division, removalAccount };
    }

    const rows = employees.map((employee) => {
        return createData(employee.nik, employee.fullname, employee.birthdate,
            employee.age, employee.division, <Button variant="contained" color="error" onClick={() => removeAccount(employee.id)}>Delete</Button>)
    });

    const removeAccount = (employeeId) => {
        axios.delete(`${process.env.REACT_APP_BASE_API}/employee/${employeeId}`)
        .then(res => {
            if(res.data.success === true) {
                toast.error(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil Menghapus Data Karyawan</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                window.location.reload();
                            }
                        }
                    )
                }
            }
        )
    }

  return (
    <div className="flex h-screen overflow-hidden bg-gray-800">
      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
      {/* Content area */}
      <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
        <main>
            <div className="px-4 sm:px-6 lg:px-8 py-8">
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>NIK</TableCell>
                                <TableCell>Full Name</TableCell>
                                <TableCell>Division</TableCell>
                                <TableCell>Age</TableCell>
                                <TableCell align="right">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.length > 0 ? rows.map((row) => (
                                <TableRow key={row.number}>
                                    <TableCell>{row.nik}</TableCell>
                                    <TableCell>{row.fullName}</TableCell>
                                    <TableCell>{row.division}</TableCell>
                                    <TableCell>
                                        {
                                            Math.abs(
                                                new Date(Date.now() - new Date(row.birthdate).getTime()).getUTCFullYear() - 1970
                                            )
                                        }
                                    </TableCell>
                                    <TableCell align="right">
                                        {row.removalAccount}
                                    </TableCell>
                                </TableRow>
                            )) :
                            <div>
                                <span>No Data</span>
                            </div>}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </main>
      </div>
    <ToastContainer />
    </div>
  )
}