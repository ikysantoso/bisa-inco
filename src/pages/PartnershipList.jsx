import React, {useEffect, useState} from 'react';
import { ThemeProvider } from "@mui/styles";
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import { Box, Button, Modal, TextField } from "@material-ui/core";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

function PartnershipList(){
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [partners, setPartners] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [partner, setPartner] = useState({});

    useEffect(() => {
        getData();
    }, []);

    let theme = createTheme();
    theme = responsiveFontSizes(theme);

    const handleDetail = (rowData, rowMeta) => {
        window.location.href = `/detail-partnership/${rowData[0]}`;
    }

    const columns = [
        {
          name: "id",
          options: {
              display: false,
              viewColumns: false,
              sort: false,
              filter: false
          }
        },
        {
            name: "companyId",
            label: "Company ID",
            options: {
                filter: true,
                sort: false
            }
        },
        {
            name: "companyName",
            label: "Company Name",
            options: {
                filter: true,
                sort: false,
            }
        },
        {
            name: "employeeTotal",
            label: "Employee",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "userTotal",
            label: "User",
            options: {
                filter: false,
                sort: false
            }
        }

    ];

    const options = {
        filterType: 'checkbox',
        selectableRows: false,
        onRowClick: handleDetail
    };

    const getData = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/partnership`)
            .then(res => {
                setPartners(res.data.data);
            });
    };

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        borderRadius: '10px',
        boxShadow: 24,
        p: 4,
    };

    const saveData = () => {
        let body = {
            companyName: partner.companyName
        }

        axios.post(`${process.env.REACT_APP_BASE_API}/company`, body)
            .then((res) => {
                if(res.data.success === true) {
                    toast.success(
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil menambah data partner</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 1000,
                            hideProgressBar: true,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                window.location.reload();
                            }
                        })
                }

                setIsModalOpen(!isModalOpen)
            })
            .catch((err) => {
                toast.error(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>Proses gagal dilakukan</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                    })

                setIsModalOpen(!isModalOpen)
            })
    }

    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
            {/* Sidebar */}
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            {/* Content area */}
            <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                {/*  Site header */}
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <main>
                    <div className="mt-2 px-4 sm:px-6 lg:px-8 py-2">
                        <Button variant="contained" color="success" onClick={() => setIsModalOpen(!isModalOpen)}>
                            <p className="font-bold">
                                Add New Partner
                            </p>
                        </Button>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-8">
                    </div>
                </main>
                <Modal
                    open={isModalOpen}
                    onClose={() => setIsModalOpen(!isModalOpen)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={modalStyle}>
                        <div className="flex flex-col gap-6">
                            <TextField
                                sx={{ my: 2 }}
                                id="outlined-basic"
                                label="Company Name"
                                variant="outlined"
                                value={partner.companyName}
                                onChange={({target}) => setPartner(state => ({...state, companyName: target.value}))}
                            />
                            <Button variant="outlined" onClick={saveData}>Save</Button>
                        </div>
                    </Box>
                </Modal>
            </div>
            <ToastContainer />
        </div>
    )
}

export default PartnershipList;