import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";
import axios from 'axios';

import Sidebar from '../partials/Sidebar';
import Header from '../partials/Header';
import Banner from '../partials/Banner';
import {setCompany} from "../redux/actions/companyActions";
// import CholesterolBarComponent from "../partials/Dashboard/component/CholesterolBar.component";
import HealthStatusPercentageComponent from "../partials/Dashboard/component/HealthStatusPercentage.component";
// import UricAcidBarComponent from "../partials/Dashboard/component/UricAcidBar.component";
// import BloodSugarBarComponent from "../partials/Dashboard/component/BloodSugarBar.component";
// import HypertensionBarComponent from "../partials/Dashboard/component/HypertensionBar.component";

import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";
// import BodyMassIndexBarComponent from "../partials/Dashboard/component/BodyMassIndexBar.component";

import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css';
import {DataGrid, GridToolbarContainer, GridToolbarExport} from "@mui/x-data-grid";
import Box from "@mui/material/Box";
import {Button, Typography} from "@mui/material";
import Modal from "@mui/material/Modal";
import Autocomplete from "@mui/material/Autocomplete";
import {TextField} from "@material-ui/core";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import {useHistory} from "react-router-dom";


am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_kelly);

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function Dashboard() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [allEmployeeData, setAllEmployeeData] = useState(null);
  const [allEmployeePercentage, setAllEmployeePercentage] = useState({});
  // const [diagnoseTotal, setDiagnoseTotal] = useState({});
  const [finishGetAllData, setFinishGetAllData] = useState(false);
  const [company, setCompany] = useState({});
  const [importMedicalCheckup, setImportMedicalCheckup] = useState([]);
  const [nakes, setNakes] = useState([]);
  const [nakesSpecial, setNakesSpecial] = useState([]);
    const [modalOpen, setModalOpen] = React.useState(false);
    const handleModalOpen = () => setModalOpen(true);
    const handleModalClose = () => setModalOpen(false);
    const [selectedNakesForSpecialAdd, setSelectedNakesForSpecialAdd] = useState({});


    useEffect(() => {
          getAllData();
      }, []);

  const getAllData = () => {
      axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
          .then(res => {
              const params = {
                  companyId: res.data.data.companyId
              }

              axios.get(`${process.env.REACT_APP_BASE_API}/company/${res.data.data.companyId}`)
                  .then(response => {
                      setCompany(response.data.data);
                  })

              axios.get(`${process.env.REACT_APP_BASE_API}/employee`, {params})
                  .then(response => {
                      setAllEmployeeData(response.data.data)
                  })

              axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/percentage`, {
                  params
              })
                  .then(res => {
                      setAllEmployeePercentage(res.data.data)
                  })
                  .catch(err => {
                      console.log(err)
                  })

              axios.get(`${process.env.REACT_APP_NAKES_BASE_API}/team/inco/${process.env.REACT_APP_NAKES_ACCESS_KEY}`)
                  .then(res => {
                      let nakes = res.data;
                      let nakesOption = [];

                      nakes.map(value => {
                          let nakesObject = {};
                          nakesObject.label =  `${value.fullname} - ${value.instance.name} - ${value.instance.address}`;
                          nakesObject.data = value;

                          nakesOption.push(nakesObject);
                      });

                      setNakes(nakesOption);
                      setSelectedNakesForSpecialAdd(nakesOption[0]);

                  })

              axios.get(`${process.env.REACT_APP_BASE_API}/nakes-special/company/${params.companyId}`)
                  .then(res => {
                      setNakesSpecial(res.data);

                      setFinishGetAllData(true);
                  })
                  .catch(err => {
                      console.log(err)
                  })

              // axios.get(`${process.env.REACT_APP_BASE_API}/medical-checkup/diagnose-total`, { params })
              //     .then(res => {
              //         let employee = res.data.data.employee;
              //         let employeeKeys = Object.keys(employee);
              //
              //         let cholesterol = res.data.data.cholesterol;
              //         let cholesterolKeys = Object.keys(cholesterol);
              //
              //         let bloodSugar = res.data.data.bloodSugar;
              //         let bloodSugarKeys = Object.keys(bloodSugar);
              //
              //         let uricAcid = res.data.data.uricAcid;
              //         let uricAcidKeys = Object.keys(uricAcid);
              //
              //         let hypertension = res.data.data.hypertension;
              //         let hypertensionKeys = Object.keys(hypertension);
              //
              //         let bodyMassIndex = res.data.data.bodyMassIndex;
              //         let bodyMassIndexKeys = Object.keys(bodyMassIndex);
              //
              //         let employeeArr = [];
              //         let cholesterolArr = [];
              //         let bloodSugarArr = [];
              //         let uricAcidArr = [];
              //         let hypertensionStageOneArr = [];
              //         let hypertensionStageTwoArr = [];
              //         let underweightArr = [];
              //         let normalArr = [];
              //         let overweightArr = [];
              //         let obesseArr = [];
              //
              //         employeeKeys.forEach((key, idx) => {
              //             employeeArr[idx] = employee[key];
              //         });
              //
              //         cholesterolKeys.forEach((key, idx) => {
              //             cholesterolArr[idx] = cholesterol[key].cholesterol ? cholesterol[key].cholesterol : 0;
              //         });
              //
              //         bloodSugarKeys.forEach((key, idx) => {
              //             bloodSugarArr[idx] = bloodSugar[key].bloodSugar ? bloodSugar[key].bloodSugar : 0;
              //         });
              //
              //         uricAcidKeys.forEach((key, idx) => {
              //             uricAcidArr[idx] = uricAcid[key].uricAcid ? uricAcid[key].uricAcid : 0;
              //         });
              //
              //         hypertensionKeys.forEach((key, idx) => {
              //             hypertensionStageOneArr[idx] = (hypertension[key].stageOne) ? hypertension[key].stageOne : 0;
              //             hypertensionStageTwoArr[idx] = (hypertension[key].stageTwo) ? hypertension[key].stageTwo : 0;
              //         });
              //
              //         bodyMassIndexKeys.forEach((key, idx) => {
              //             underweightArr[idx] = (bodyMassIndex[key].underweight) ? bodyMassIndex[key].underweight : 0;
              //             normalArr[idx] = (bodyMassIndex[key].normal) ? bodyMassIndex[key].normal : 0;
              //             overweightArr[idx] = (bodyMassIndex[key].overweight) ? bodyMassIndex[key].overweight : 0;
              //             obesseArr[idx] = (bodyMassIndex[key].obesse) ? bodyMassIndex[key].obesse : 0;
              //         });
              //
              //         let objectDiagnoseTotal = {
              //             employee: employeeArr,
              //             cholesterol: cholesterolArr,
              //             bloodSugar: bloodSugarArr,
              //             uricAcid: uricAcidArr,
              //             hypertensionStageOne: hypertensionStageOneArr,
              //             hypertensionStageTwo: hypertensionStageTwoArr,
              //             underweight: underweightArr,
              //             normal: normalArr,
              //             overweight: overweightArr,
              //             obesse: obesseArr
              //         }
              //
              //         setDiagnoseTotal(objectDiagnoseTotal);
              //     })
          })
  }

  const saveNakesSpecial = () => {
      axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
          .then(res => {
              let inputAccess = {
                  company_id: res.data.data.companyId,
                  nursing_number: selectedNakesForSpecialAdd.nursing_number,
                  fullname: selectedNakesForSpecialAdd.fullname,
                  nik: selectedNakesForSpecialAdd.nik,
                  phone: selectedNakesForSpecialAdd.phone,
                  instance_name: selectedNakesForSpecialAdd.instance.name,
                  instance_address: selectedNakesForSpecialAdd.instance.address,
                //   action: res.data.data.deleteNurse,
              }

              axios.post(`${process.env.REACT_APP_BASE_API}/nakes-special`, inputAccess)
                  .then((res) => {
                      window.location.reload();
                  })
          })
  }

  const deleteNurse = (id) => {
    axios.delete(`${process.env.REACT_APP_BASE_API}/nakes-special/${id}`, {
        id: id,
    })
        .then(res => {
            if(res.data.success === true) {
                toast.success(
                    <div className='flex flex-col space-y-2'>
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Berhasil delete</h1>
                    </div>,
                    {
                        position: "bottom-right",
                        autoClose: 2000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                        onClose: () => {
                            window.location.reload();
                        }
                    }
                )
            }
        }
    )
}

  if (finishGetAllData === false) {
      return (
          <div className="flex h-screen overflow-hidden bg-gray-800">
              <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
              <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                  <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                  <main>
                      <div className="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
                          <Skeleton count={3} />
                      </div>
                  </main>
                  <Banner />
              </div>
          </div>
      );
  } else {
      const nakesSpecialRows = nakesSpecial.map((data) => {
          return {
              id: data.id,
              fullName: data.fullname,
              nik: data.nik,
              nursingNumber: data.nursing_number,
              phone: data.phone,
              instanceName: data.instance_name,
              instanceAddress: data.instance_address
          }
      })

      const nakesSpecialColumns = [
          { field: 'id', headerName: 'ID', width: 70 },
          {
              field: 'fullName',
              headerName: 'Nama Lengkap',
              width: 150,
          },
          {
              field: 'nik',
              headerName: 'NIK',
              width: 150,
          },
          {
              field: 'nursingNumber',
              headerName: 'Nomor Keperawatan',
              width: 150,
          },
          {
              field: 'phone',
              headerName: 'Nomor Telepon',
              width: 150,
          },
          {
              field: 'instanceName',
              headerName: 'Tempat Bertugas',
              width: 200,
          },
          {
              field: 'instanceAddress',
              headerName: 'Alamat Tempat Bertugas',
              width: 300,
          },
          {
            field: 'deleteNurse',
            headerName: 'Action',
            width: 150,
              renderCell: (params) => {
                  return <Button variant="outlined" onClick={() => deleteNurse(params.id)}>Delete</Button>;
              }
          }
      ];

      return (
          <div className="flex h-screen overflow-hidden bg-gray-800">
              <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
              <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                  <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                  <main>
                      <div className="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
                          <div className="flex flex-col gap-6">
                              <div>
                                  <span className="text-white font-bold text-3xl">{company.companyName}</span>
                              </div>
                              <div className="gap-6">
                                  <HealthStatusPercentageComponent employeeData={allEmployeeData} allEmployeePercentage={allEmployeePercentage} />
                              </div>
                              {/*<div className="flex flex-row space-x-4 mt-16 justify-start items-center">*/}
                              {/*    <span className="text-white font-bold">*/}
                              {/*        Nakes Spesial untuk Perusahaan ini*/}
                              {/*    </span>*/}
                              {/*    <Button variant="contained" color="success" onClick={() => handleModalOpen()}>*/}
                              {/*        Pilih Nakes*/}
                              {/*    </Button>*/}
                              {/*</div>*/}
                              {/*<div className="mb-8">*/}
                              {/*    <Box sx={{ height: 400, width: '100%', backgroundColor: '#ffffff' }}>*/}
                              {/*        <DataGrid*/}
                              {/*            rows={nakesSpecialRows}*/}
                              {/*            columns={nakesSpecialColumns}*/}
                              {/*        />*/}
                              {/*    </Box>*/}
                              {/*</div>*/}
                              {/*<CholesterolBarComponent diagnoseTotal={diagnoseTotal} />*/}
                              {/*<HypertensionBarComponent diagnoseTotal={diagnoseTotal} />*/}
                              {/*<BloodSugarBarComponent diagnoseTotal={diagnoseTotal} />*/}
                              {/*<UricAcidBarComponent diagnoseTotal={diagnoseTotal} />*/}
                              {/*<BodyMassIndexBarComponent diagnoseTotal={diagnoseTotal} />*/}
                          </div>
                      </div>
                  </main>
                  <Banner />
              </div>

              <Modal
                  open={modalOpen}
                  onClose={handleModalClose}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
              >
                  <Box sx={modalStyle}>
                      <div>
                          <Autocomplete
                              disablePortal
                              id="combo-box-demo"
                              options={nakes}
                              sx={{ width: 240 }}
                              onChange={(event, value) => setSelectedNakesForSpecialAdd(value.data)}
                              renderInput={(params) => <TextField {...params} variant="outlined" label="Nakes" />}
                          />
                      </div>
                      <div style={{ display: "flex", justifyContent: "flex-start", gap: "10px", marginTop: "10px" }}>
                        <div>
                            <Button
                                className="btn-upload"
                                color="primary"
                                variant="outlined"
                                component="span"
                                onClick={() => handleModalClose()}>
                                Close
                            </Button>
                        </div>
                        <div>
                            <Button
                                className="btn-upload"
                                color="success"
                                variant="contained"
                                component="span"
                                onClick={() => saveNakesSpecial()}>
                                Save
                            </Button>
                        </div>
                      </div>
                  </Box>
              </Modal>
          </div>
      )
  }

}

const mapDispatchToProps = (dispatch) => ({
    setCompany: (payload) => dispatch(setCompany(payload))
});

export default connect(null, mapDispatchToProps)(Dashboard);