import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import axios from 'axios';

import {Button, Modal, TextField} from "@material-ui/core";
import Box from "@mui/material/Box";
import Autocomplete from '@mui/material/Autocomplete';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        "& .MuiInputLabel-outlined:not(.MuiInputLabel-shrink)": {
            // Default transform is "translate(14px, 20px) scale(1)""
            // This lines up the label with the initial cursor position in the input
            // after changing its padding-left.
            transform: "translate(34px, 20px) scale(1);"
        }
    },
    inputRoot: {
        color: "black",
        backgroundColor: "white",
        // This matches the specificity of the default styles at https://github.com/mui-org/material-ui/blob/v4.11.3/packages/material-ui-lab/src/Autocomplete/Autocomplete.js#L90
        '&[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input:first-child': {
            // Default left padding is 6px
            paddingLeft: 26
        },
        "& .MuiOutlinedInput-notchedOutline": {
            borderColor: "white"
        },
        "&:hover .MuiOutlinedInput-notchedOutline": {
            borderColor: "white"
        },
        "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
            borderColor: "white"
        }
    }
}));

export default function Login() {
    const history = useHistory()
    const [inputAccess, setInputAccess] = useState({})
    const [passwordShown, setPasswordShown] = useState(false)
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [companySelectionOption, setCompanySelectionOption] = useState([]);
    const [selectedCompany, setSelectedCompany] = useState("");
    const [selectedCompanyForReset, setSelectedCompanyForReset] = useState("");
    const [emailForReset, setEmailForReset] = useState("");

    const classesStyle = useStyles();

    useEffect(() => {
        fetchAllCompanies();
    }, []);

    const fetchAllCompanies = () => {
        axios.get(`${process.env.REACT_APP_BASE_API}/company`)
            .then((res) => {
                let companies = res.data.data;
                let companyOption = [];

                companies.map(value => {
                    let companyObject = {};
                    companyObject.label = value.companyName;
                    companyObject.id = value.id;

                    companyOption.push(companyObject);
                });

                setCompanySelectionOption(companyOption);
            })
    }

    const doSignIn = (e) => {
        e.preventDefault()

        if (Object.keys(inputAccess).length === 0 || (inputAccess.email === ('' || undefined)) || (inputAccess.password === ('' || undefined))) {
            toast.error(
                <h1 className='text-md' style={{fontWeight: 'bold'}}>Email atau Password tidak boleh kosong</h1>,
                {
                    position: "bottom-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: false,
                    draggable: false,
                    progress: undefined,
                })
        } else if (selectedCompany === "") {
            toast.error(
                <h1 className='text-md' style={{fontWeight: 'bold'}}>Company tidak boleh kosong</h1>,
                {
                    position: "bottom-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: false,
                    draggable: false,
                    progress: undefined,
                })
        } else {
            inputAccess.companyId = selectedCompany;

            axios.post(`${process.env.REACT_APP_BASE_API}/auth/login`, inputAccess)
                .then((res) => {
                    const sessionToken = res.data.data.sessionToken
                    const id = res.data.data.id

                    localStorage.setItem("sessionToken", sessionToken)
                    localStorage.setItem("id", id)

                    toast.success(
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Sign-in berhasil</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 1000,
                            hideProgressBar: true,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                            onClose: () => {
                                history.push("/")
                            }
                        })
                })
                .catch((err) => {
                    toast.error(
                        <h1 className='text-md' style={{fontWeight: 'bold'}}>Email atau Password salah</h1>,
                        {
                            position: "bottom-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: false,
                            draggable: false,
                            progress: undefined,
                        })
                })
        }
    }

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        borderRadius: '10px',
        boxShadow: 24,
        p: 4,
    };

    const requestResetPassword = () => {
        if (selectedCompanyForReset === "") {
            toast.error(
                <h1 className='text-md' style={{fontWeight: 'bold'}}>Perusahaan tidak boleh kosong</h1>,
                {
                    position: "bottom-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: false,
                    draggable: false,
                    progress: undefined,
                })
        } else {
            if (emailForReset === "") {
                toast.error(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>Email tidak boleh kosong</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                    })
            } else {
                let inputRequestResetPassword = {
                    email: emailForReset,
                    company_id: selectedCompanyForReset
                }

                axios.post(`${process.env.REACT_APP_BASE_API}/auth/reset-password/request`, inputRequestResetPassword)
                    .then((res) => {
                        toast.success(
                            <h1 className='text-md' style={{fontWeight: 'bold'}}>Request berhasil</h1>,
                            {
                                position: "bottom-right",
                                autoClose: 1000,
                                hideProgressBar: true,
                                closeOnClick: false,
                                pauseOnHover: false,
                                draggable: false,
                                progress: undefined,
                                onClose: () => {
                                    history.push("/")
                                }
                            })
                    })
                    .catch((err) => {
                        let errorData = err.response.data;
                        let errorMessage;

                        console.log(errorData.data);
                        if (errorData.data === "Wait for one hour to next request") {
                            errorMessage = "Request bisa dilakukan kembali setelah 1 jam ke depan.";
                        } else if (errorData.data === "user not exist") {
                            errorMessage = "User tidak terdaftar";
                        } else {
                            errorMessage = "Request gagal";
                        }

                        toast.error(
                            <h1 className='text-md' style={{fontWeight: 'bold'}}>{errorMessage}</h1>,
                            {
                                position: "bottom-right",
                                autoClose: 3000,
                                hideProgressBar: false,
                                closeOnClick: false,
                                pauseOnHover: false,
                                draggable: false,
                                progress: undefined,
                            })
                    })
            }
        }

    }
    return (
        <div className="bg-gray-800">
            <div className="flex flex-col items-center justify-center h-screen space-y-8" style={{fontFamily: 'Roboto'}}>
                <div className="flex flex-row items-center mt-16">
                    <h2 className="font-bold text-3xl text-white">BISA</h2>
                    <h2 className="font-bold text-3xl text-white" style={{fontFamily: 'Georgia', marginLeft: '10px'}}>inco</h2>
                </div>

                <form className="flex flex-col items-center justify-center space-y-6">
                    <div>
                        <Autocomplete
                            disablePortal
                            id="combo-box-demo"
                            options={companySelectionOption}
                            sx={{ width: 240 }}
                            onChange={(event, value) => setSelectedCompany(value.id)}
                            classes={{inputRoot: classesStyle.inputRoot}}
                            renderInput={(params) => <TextField  id="outlined-basic" variant="outlined" {...params} label="Pilih Company" />}
                        />
                    </div>

                    <div className="relative mt-1">
                        <input type="text" className="h-10 p-4 text-sm font-light border border-gray-200 rounded w-60 focus:outline-none focus:border-blue-400" placeholder="Email" onChange={({target}) => setInputAccess(state => ({...state, email: target.value}))} value={inputAccess.email || ''} />
                    </div>

                    <div className="relative mt-1 mb-2">
                        <div className="flex flex-col items-end space-y-2">
                            <input type={passwordShown ? 'text' : 'password'} className="h-10 p-4 text-sm font-light border border-gray-200 rounded w-60 focus:outline-none focus:border-blue-400" placeholder="Password" onChange={({target}) => setInputAccess(state => ({...state, password: target.value}))} value={inputAccess.password || ''} />

                            <button type="button" onClick={(e) => setPasswordShown(!passwordShown)} className="text-sm text-white">
                                {passwordShown ? 'Hide Password' : 'Show Password'}
                            </button>
                        </div>
                    </div>

                    <button className="text-blue-500 border-2 border-blue-500 rounded-lg hover:bg-blue-500 hover:text-white w-60 h-9" onClick={doSignIn}>Sign in</button>

                    <button type="button" onClick={() => setIsModalOpen(!isModalOpen)} className="text-sm text-white">Forgot password</button>
                </form>

                <Modal
                        open={isModalOpen}
                        onClose={() => setIsModalOpen(!isModalOpen)}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={modalStyle}>
                            <div className="flex flex-col gap-6">
                                <div>
                                    <Autocomplete
                                        disablePortal
                                        id="combo-box-demo"
                                        options={companySelectionOption}
                                        sx={{ width: 240 }}
                                        onChange={(event, value) => setSelectedCompanyForReset(value.id)}
                                        classes={{inputRoot: classesStyle.inputRoot}}
                                        renderInput={(params) => <TextField  id="outlined-basic" variant="outlined" {...params} label="Pilih Company" />}
                                    />
                                </div>
                                <TextField
                                        sx={{ my: 2 }}
                                        id="outlined-basic"
                                        label="Email"
                                        variant="outlined"
                                        onChange={(event) => setEmailForReset(event.target.value.trim())}
                                    />
                                <Button variant="outlined" onClick={() => requestResetPassword()}>Kirim</Button>
                            </div>
                        </Box>
                </Modal>
            </div>

            <div className="absolute bottom-0 left-0" style={{marginLeft: '15px'}}>
                <span className="text-sm text-white">Powered By</span>
                <div className="flex flex-row items-center span-x-8">
                    <img className="h-8 w-8" style={{marginBottom: '10px', marginTop: '10px'}} src={process.env.PUBLIC_URL+"favicon.ico"} />
                    <span className="font-bold text-md text-white" style={{marginLeft: '10px'}}>
                        Bersama Indonesia Sehat Alami
                    </span>
                </div>
            </div>

            <ToastContainer />
        </div>
    )
}