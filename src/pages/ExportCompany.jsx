import React, {useEffect, useState} from 'react';
import { ThemeProvider } from "@mui/styles";
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import Header from "../partials/Header";
import Sidebar from "../partials/Sidebar";
import axios from "axios";
import { saveAs } from "file-saver";
import XlsxPopulate from "xlsx-populate";
import { Box, Button, Modal, TextField } from "@material-ui/core";

import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

export default function ExportCompany() {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    function getSheetData(data, header) {
      var fields = Object.keys(data[0]);
      var sheetData = data.map(function (row) {
        return fields.map(function (fieldName) {
          return row[fieldName] ? row[fieldName] : "";
        });
      });
      sheetData.unshift(header);
      return sheetData;
    }
  
    async function saveAsExcel() {
      var data = [
        { nama_perusahaan: "PT BISA", nama_lengkap: "Candra Afrian", tanggal_lahir: "14/04/2001", jenis_kelamin: "Laki-Laki", divisi: "IT", email: "tes@gmail.com", no_hp: "08123456789", nik: "3123456789876543", berat_badan: "100", tinggi_badan: "175", tekanan_darah_atas: "20", tekanan_darah_bawah: "50", gula_darah: "60", asam_urat: "100"},
        { nama_perusahaan: "PT HONDA", nama_lengkap: "Candra", tanggal_lahir: "14/05/2002", jenis_kelamin: "Laki-Laki", divisi: "Admin", email: "tes1@gmail.com", no_hp: "089876535361", nik: "3537528634921673", berat_badan: "70", tinggi_badan: "150", tekanan_darah_atas: "30", tekanan_darah_bawah: "80", gula_darah: "20", asam_urat: "200"},
        { nama_perusahaan: "PT YAMAHA", nama_lengkap: "Afrian", tanggal_lahir: "14/06/2003", jenis_kelamin: "Laki-Laki", divisi: "Marketing", email: "tes2@gmail.com", no_hp: "08546543433", nik: "3735472541876325", berat_badan: "50", tinggi_badan: "110", tekanan_darah_atas: "10", tekanan_darah_bawah: "30", gula_darah: "80", asam_urat: "300"}
      ];
      let header = ["Nama Perusahaan", "Nama Lengkap", "Tanggal Lahir", "Jenis Kelamin", "Divisi", "Email", "No HP", "NIK", "Berat Badan (kg)", "Tinggi Badan (cm)", "Tekanan Darah Atas", "Tekanan Darah Bawah", "Gula Darah", "Kolesterol", "Asam Urat"];
  
      XlsxPopulate.fromBlankAsync().then(async (workbook) => {
        const sheet1 = workbook.sheet(0);
        const sheetData = getSheetData(data, header);
        const totalColumns = sheetData[0].length;
  
        sheet1.cell("A1").value(sheetData);
        const range = sheet1.usedRange();
        const endColumn = String.fromCharCode(64 + totalColumns);
        sheet1.row(1).style("bold", true);
        sheet1.range("A1:" + endColumn + "1").style("fill", "BFBFBF");
        range.style("border", true);
        return workbook.outputAsync().then((res) => {
          saveAs(res, "file.xlsx");
        });
      });
    }
  
    
    return (
        <div className="flex h-screen overflow-hidden bg-gray-800">
            {/* Sidebar */}
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            {/* Content area */}
            <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
                {/*  Site header */}
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                <main>
                    <div className="mt-2 px-4 sm:px-6 lg:px-8 py-2">
                        <Button variant="contained" onClick={saveAsExcel} color="primary" >Excel</Button>
                    </div>
                </main>
            </div>
            <ToastContainer />
        </div>
    )
}
