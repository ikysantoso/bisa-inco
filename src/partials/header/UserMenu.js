import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Transition from '../../utils/Transition';
import axios from 'axios';
import {Button, Modal, TextField} from "@material-ui/core";
import Box from "@mui/material/Box";
import UserAvatar from '../../images/user-avatar-32.png';
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";


function UserMenu({userData}) {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [passwordValue, setPasswordValue] = useState({});

  const trigger = useRef(null);
  const dropdown = useRef(null);

  // close on click outside
  useEffect(() => {
    const clickHandler = ({ target }) => {
      if (!dropdownOpen || dropdown.current.contains(target) || trigger.current.contains(target)) return;
      setDropdownOpen(false);
    };
    document.addEventListener('click', clickHandler);
    return () => document.removeEventListener('click', clickHandler);
  });

  // close if the esc key is pressed
  useEffect(() => {
    const keyHandler = ({ keyCode }) => {
      if (!dropdownOpen || keyCode !== 27) return;
      setDropdownOpen(false);
    };
    document.addEventListener('keydown', keyHandler);
    return () => document.removeEventListener('keydown', keyHandler);
  });

  const doSignOut = () => {
    const body = {
      id: localStorage.getItem('id')
    }

    axios.post(`${process.env.REACT_APP_BASE_API}/auth/logout`, body)
        .then((res) => {
          localStorage.removeItem('id')
          window.location.href = '/';
        })
  }

  const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    borderRadius: '10px',
    boxShadow: 24,
    p: 4,
  };

  const doChangePassword = () => {
    if (passwordValue.newPassword !== passwordValue.confirmNewPassword) {
      toast.error(
          <h1 className='text-md' style={{fontWeight: 'bold'}}>Konfirmasi Password Baru tidak sesuai</h1>,
          {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: false,
            draggable: false,
            progress: undefined,
          })
    } else {
        let changePasswordInput = {
            id: localStorage.getItem("id"),
            old_password: passwordValue.oldPassword,
            new_password: passwordValue.confirmNewPassword
        }

        axios.post(`${process.env.REACT_APP_BASE_API}/auth/change-password`, changePasswordInput)
            .then((res) => {
                toast.success(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>{res.data.message}</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 1000,
                        hideProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                        onClose: () => {
                            window.location.reload()
                        }
                    })
            })
            .catch((err) => {
                toast.error(
                    <h1 className='text-md' style={{fontWeight: 'bold'}}>{err.response.data.message}</h1>,
                    {
                        position: "bottom-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: false,
                        draggable: false,
                        progress: undefined,
                    })
            })
    }
  }

  return (
    <div className="relative inline-flex">
      <button
        ref={trigger}
        className="inline-flex justify-center items-center group"
        aria-haspopup="true"
        onClick={() => setDropdownOpen(!dropdownOpen)}
        aria-expanded={dropdownOpen}
      >
        <img className="w-8 h-8 rounded-full" src={UserAvatar} width="32" height="32" alt="User" />
        <div className="flex items-center truncate">
          <span className="truncate ml-2 text-sm font-medium text-white group-hover:text-gray-500">{userData.name}</span>
          <svg className="w-3 h-2 shrink-0 ml-1 fill-current text-white" viewBox="0 0 12 12">
            <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
          </svg>
        </div>
      </button>

      <Transition
        className="origin-top-right z-10 absolute top-full right-0 min-w-44 bg-white border border-gray-200 py-1.5 rounded shadow-lg overflow-hidden mt-1"
        show={dropdownOpen}
        enter="transition ease-out duration-200 transform"
        enterStart="opacity-0 -translate-y-2"
        enterEnd="opacity-100 translate-y-0"
        leave="transition ease-out duration-200"
        leaveStart="opacity-100"
        leaveEnd="opacity-0"
      >
        <div
          ref={dropdown}
          onFocus={() => setDropdownOpen(true)}
          onBlur={() => setDropdownOpen(false)}
        >
          <div className="pt-0.5 pb-2 px-3 mb-1 border-b border-gray-200">
            <div className="font-medium text-gray-800">{userData.name}</div>
            <div className="text-xs text-gray-500 italic">{userData.role}</div>
          </div>
          <ul>
            <li>
              <Link className="font-medium text-sm text-indigo-500 hover:text-indigo-600 flex items-center py-1 px-3" to="/" onClick={() => setIsModalOpen(!isModalOpen)} >
                Change Password
              </Link>
            </li> 
            <li>
              <Link
                className="font-medium text-sm text-indigo-500 hover:text-indigo-600 flex items-center py-1 px-3"
                to="/"
                onClick={() => doSignOut()}
              >
                Sign Out
              </Link>
            </li>
          </ul>
        </div>
      </Transition>
        <Modal
            open={isModalOpen}
            onClose={() => setIsModalOpen(!isModalOpen)}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
            <Box sx={modalStyle}>
              <div className="flex flex-col gap-6">
                  <TextField
                      sx={{ my: 2 }}
                      id="outlined-basic"
                      label="Password Lama"
                      variant="outlined"
                      onChange={({target}) => setPasswordValue(state => ({...state, oldPassword: target.value}))}
                  />

                <TextField
                  sx={{ my: 2 }}
                  id="outlined-basic"
                  label="Password Baru"
                  variant="outlined"
                  onChange={({target}) => setPasswordValue(state => ({...state, newPassword: target.value}))}
                />

                <TextField
                  sx={{ my: 2 }}
                  id="outlined-basic"
                  label="Konfirmasi Password Baru"
                  variant="outlined"
                  onChange={({target}) => setPasswordValue(state => ({...state, confirmNewPassword: target.value}))}
                />

                <Button variant="outlined" onClick={doChangePassword}>Simpan</Button>
              </div>
            </Box>
        </Modal>

      <ToastContainer />
    </div>
  )
}

export default UserMenu;