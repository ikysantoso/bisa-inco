import React from 'react';
import BarChart from '../../../charts/BarChart03';

import { tailwindConfig } from '../../../utils/Utils';

function HealthStatusPercentageComponent({ employeeData, allEmployeePercentage }) {
    if ((allEmployeePercentage.cholesterol) && (allEmployeePercentage.stageOneHypertension)
        && (allEmployeePercentage.stageTwoHypertension) && (allEmployeePercentage.bloodSugar)
        && (allEmployeePercentage.uricAcid) && (allEmployeePercentage.underweight)
        && (allEmployeePercentage.dangerScoreDiagnose) && (allEmployeePercentage.healthy)
        && (allEmployeePercentage.oneDangerScoreDiagnose) && (allEmployeePercentage.twoDangerScoreDiagnose)
        && (allEmployeePercentage.threeDangerScoreDiagnose) && (allEmployeePercentage.fourDangerScoreDiagnose)
        && (allEmployeePercentage.fiveDangerScoreDiagnose) && (allEmployeePercentage.zeroDangerScoreDiagnose)
        && (allEmployeePercentage.notHealthy)) {
        let chartData = {
            labels: ['Health Status'],
            employee: employeeData.length,
            datasets: [
                {
                    label: 'Underweight',
                    data: [Math.round(allEmployeePercentage.underweight.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Overweight',
                    data: [Math.round(allEmployeePercentage.overweight.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Obese',
                    data: [Math.round(allEmployeePercentage.obese.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Hipertensi Stage 1',
                    data: [Math.round(allEmployeePercentage.stageOneHypertension.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[800],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[900],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Hipertensi Stage 2',
                    data: [Math.round(allEmployeePercentage.stageTwoHypertension.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[900],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[1000],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Gula Darah Tinggi',
                    data: [Math.round(allEmployeePercentage.bloodSugar.counter)],
                    backgroundColor: tailwindConfig().theme.colors.blue[500],
                    hoverBackgroundColor: tailwindConfig().theme.colors.blue[600],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Asam Urat Tinggi',
                    data: [Math.round(allEmployeePercentage.uricAcid.counter)],
                    backgroundColor: tailwindConfig().theme.colors.blue[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.gray[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Hiperkolesterol',
                    data: [Math.round(allEmployeePercentage.cholesterol.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Tidak Bugar',
                    data: [Math.round(allEmployeePercentage.notHealthy.counter)],
                    backgroundColor: tailwindConfig().theme.colors.indigo[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.indigo[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Karyawan yang benar-benar (sehat dan tidak bugar)',
                    data: [Math.round(allEmployeePercentage.zeroDangerScoreDiagnose.counter)],
                    backgroundColor: tailwindConfig().theme.colors.red[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.red[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Karyawan yang benar-benar (sehat dan bugar)',
                    data: [Math.round(allEmployeePercentage.healthy.counter)],
                    backgroundColor: tailwindConfig().theme.colors.green[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.green[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Indikator Bahaya Kesehatan 1',
                    data: [Math.round(allEmployeePercentage.oneDangerScoreDiagnose.counter)],
                    backgroundColor: tailwindConfig().theme.colors.red[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.red[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Indikator Bahaya Kesehatan 2',
                    data: [Math.round(allEmployeePercentage.twoDangerScoreDiagnose.counter)],
                    backgroundColor: tailwindConfig().theme.colors.red[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.red[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Indikator Bahaya Kesehatan 3',
                    data: [Math.round(allEmployeePercentage.threeDangerScoreDiagnose.counter)],
                    backgroundColor: tailwindConfig().theme.colors.red[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.red[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Indikator Bahaya Kesehatan 4',
                    data: [Math.round(allEmployeePercentage.fourDangerScoreDiagnose.counter)],
                    backgroundColor: tailwindConfig().theme.colors.red[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.red[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Indikator Bahaya Kesehatan 5',
                    data: [Math.round(allEmployeePercentage.fiveDangerScoreDiagnose.counter)],
                    backgroundColor: tailwindConfig().theme.colors.red[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.red[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                },
                {
                    label: 'Indikator Bahaya kesehatan lebih dari atau sama dengan 3',
                    data: [Math.round(allEmployeePercentage.dangerScoreDiagnose.counter)],
                    backgroundColor: tailwindConfig().theme.colors.red[700],
                    hoverBackgroundColor: tailwindConfig().theme.colors.red[800],
                    barPercentage: 1,
                    categoryPercentage: 1,
                    hidden: true,
                }
            ],
        };

        return (
            <div className="col-span-full xl:col-span-6 bg-white rounded-lg">
                <header className="px-5 py-4">
                    <h2 className="font-semibold text-gray-800">Status Kesehatan Karyawan</h2>
                </header>
                <div className="px-5 py-3">
                    <div className="flex items-start">
                        <div className="text-3xl font-bold text-gray-800 mr-2">{employeeData.length} Karyawan</div>
                    </div>
                </div>
                <div className="grow">
                    <BarChart data={chartData} width={595} height={40}/>
                </div>
            </div>
        );
    }

    return (
        <div>
            <h1>Loading</h1>
        </div>
    )
}

export default HealthStatusPercentageComponent;
