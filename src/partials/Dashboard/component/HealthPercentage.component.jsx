import React, {useEffect, useRef} from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

function HealthPercentageComponent({ diagnoseTotal }) {
    const chartRef = useRef(null);

    const CHART_ID = 'health_percentage_chart';

    // Add data
    let data = [
        {
            country: "Hipertensi Stage 1",
            litres: 501.9
        },
        {
            country: "Hipertensi Stage 2",
            litres: 301.9
        },
        {
            country: "Kolesterol",
            litres: 201.1
        },
        {
            country: "Asam Urat",
            litres: 165.8
        },
        {
            country: "Gula Darah",
            litres: 139.9
        }
    ];

    useEffect(() => {
        if (!chartRef.current) {
            chartRef.current = am4core.create(CHART_ID, am4charts.PieChart3D);
            chartRef.current.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chartRef.current.data = data;

            let series = chartRef.current.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = "litres";
            series.dataFields.category = "country";
            series.labels.template.disabled = true;

            chartRef.current.legend = new am4charts.Legend()
        }

        return () => {
            chartRef.current && chartRef.current.dispose();
        };
    }, []);

    React.useEffect(() => {
        if (chartRef.current) {
            chartRef.current.data = data;
        }
    }, [data]);

    // Handle component unmounting, dispose chart
    React.useEffect(() => {
        return () => {
            chartRef.current && chartRef.current.dispose();
        };
    }, []);

    return (
        <div className="flex flex-col col-span-full sm:col-span-6 bg-white shadow-lg rounded-lg border border-gray-200">
            <header className="px-5 py-4">
                <h2 className="font-semibold text-gray-800">Health Percentage</h2>
            </header>
            <div
                id={CHART_ID}
                style={{
                    width: '100%',
                    height: '300px',
                    margin: '50px 0'
                }}
            />
        </div>
    );
}

export default HealthPercentageComponent;
