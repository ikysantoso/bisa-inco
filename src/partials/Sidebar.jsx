import React, { useState, useEffect, useRef } from 'react';
import { NavLink, useLocation } from 'react-router-dom';

import SidebarLinkGroup from './SidebarLinkGroup';
import axios from "axios";

function Sidebar({
 sidebarOpen,
 setSidebarOpen
}) {

  const location = useLocation();
  const { pathname } = location;

  const trigger = useRef(null);
  const sidebar = useRef(null);

  const storedSidebarExpanded = localStorage.getItem('sidebar-expanded');
  const [sidebarExpanded, setSidebarExpanded] = useState(storedSidebarExpanded === null ? false : storedSidebarExpanded === 'true');

  const [ companyId, setCompanyId ] = useState('');
  const [ companyData, setCompanyData ] = useState({});

  useEffect(() => {
    const clickHandler = ({ target }) => {
      if (!sidebar.current || !trigger.current) return;
      if (!sidebarOpen || sidebar.current.contains(target) || trigger.current.contains(target)) return;
      setSidebarOpen(false);
    };
    document.addEventListener('click', clickHandler);
    return () => document.removeEventListener('click', clickHandler);
  });

  useEffect(() => {
    const keyHandler = ({ keyCode }) => {
      if (!sidebarOpen || keyCode !== 27) return;
      setSidebarOpen(false);
    };

    document.addEventListener('keydown', keyHandler);

    return () => document.removeEventListener('keydown', keyHandler);
  });

  useEffect(() => {
    localStorage.setItem('sidebar-expanded', sidebarExpanded);

    if (sidebarExpanded) {
      document.querySelector('body').classList.add('sidebar-expanded');
    } else {
      document.querySelector('body').classList.remove('sidebar-expanded');
    }

    getUserData()
  }, [sidebarExpanded]);

  const getUserData = async () => {
    await axios.get(`${process.env.REACT_APP_BASE_API}/user/${localStorage.getItem('id')}`)
        .then(res => {
          setCompanyId(res.data.data.company);
          setCompanyData(res.data.data.companyData);
        })
  }

  return (
      <div>
        {/* Sidebar backdrop (mobile only) */}
        <div className={`fixed inset-0 bg-gray-900 bg-opacity-30 z-40 lg:hidden lg:z-auto transition-opacity duration-200 ${sidebarOpen ? 'opacity-100' : 'opacity-0 pointer-events-none'}`} aria-hidden="true"></div>

        {/* Sidebar */}
        <div
            id="sidebar"
            ref={sidebar}
            className={`flex flex-col absolute z-40 left-0 top-0 lg:static lg:left-auto lg:top-auto lg:translate-x-0 transform h-screen overflow-y-scroll lg:overflow-y-auto no-scrollbar w-64 lg:w-20 lg:sidebar-expanded:!w-64 2xl:!w-64 shrink-0 bg-gray-900 p-4 transition-all duration-200 ease-in-out ${sidebarOpen ? 'translate-x-0' : '-translate-x-64'}`}
        >

          {/* Sidebar header */}
          <div className="flex justify-between mb-4 pr-3 sm:px-2">
            {/* Close button */}
            <button
                ref={trigger}
                className="lg:hidden text-gray-500 hover:text-gray-400"
                onClick={() => setSidebarOpen(!sidebarOpen)}
                aria-controls="sidebar"
                aria-expanded={sidebarOpen}
            >
              <span className="sr-only">Close sidebar</span>
              <svg className="w-6 h-6 fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.7 18.7l1.4-1.4L7.8 13H20v-2H7.8l4.3-4.3-1.4-1.4L4 12z" />
              </svg>
            </button>
            {/* Logo */}
            <NavLink exact to="/" className="block">
              <div className="flex flex-row items-center space-x-2" style={{marginBottom: '6px', marginTop: '10px'}}>
                <h2 className="font-bold text-2xl text-white">BISA</h2>
                <h2 className="font-bold text-2xl text-white" style={{fontFamily: 'Georgia', marginLeft: '5px'}}>inco</h2>
              </div>
              <div className="flex flex-row space-x-1" style={{marginBottom: '10px'}}>
                  <span className="text-gray-500">
                    {process.env.REACT_APP_APPLICATION_MAJOR_VERSION}
                  </span>
                <span className="text-gray-500">.</span>
                <span className="text-gray-500">
                    {process.env.REACT_APP_APPLICATION_MINOR_VERSION}
                  </span>
                <span className="text-gray-500">.</span>
                <span className="text-gray-500">
                    {process.env.REACT_APP_APPLICATION_PATCH_VERSION}
                  </span>
              </div>
            </NavLink>
          </div>

          {/* Links */}
          <div className="space-y-8">
            {/* Pages group */}
            <div>
              <h3 className="text-xs uppercase text-gray-500 font-semibold pl-3">
                <span className="hidden lg:block lg:sidebar-expanded:hidden 2xl:hidden text-center w-6" aria-hidden="true">•••</span>
                <span className="lg:hidden lg:sidebar-expanded:block 2xl:block">Pages</span>
              </h3>
              <ul className="mt-3">
                {/* Dashboard */}
                <li className={`px-3 py-2 rounded-sm mb-0.5 last:mb-0 ${pathname === '/' && 'bg-gray-900'}`}>
                  <NavLink exact to="/" className={`block text-gray-200 hover:text-white truncate transition duration-150 ${pathname === '/dashboard' && 'hover:text-gray-200'}`}>
                    <div className="flex items-center">
                      <svg className="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                        <path className={`fill-current text-gray-400 ${pathname === '/' && '!text-indigo-500'}`} d="M12 0C5.383 0 0 5.383 0 12s5.383 12 12 12 12-5.383 12-12S18.617 0 12 0z" />
                        <path className={`fill-current text-gray-600 ${pathname === '/' && 'text-indigo-600'}`} d="M12 3c-4.963 0-9 4.037-9 9s4.037 9 9 9 9-4.037 9-9-4.037-9-9-9z" />
                        <path className={`fill-current text-gray-400 ${pathname === '/' && 'text-indigo-200'}`} d="M12 15c-1.654 0-3-1.346-3-3 0-.462.113-.894.3-1.285L6 6l4.714 3.301A2.973 2.973 0 0112 9c1.654 0 3 1.346 3 3s-1.346 3-3 3z" />
                      </svg>
                      <span className="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Dashboard</span>
                    </div>
                  </NavLink>
                </li>
                <SidebarLinkGroup activecondition={pathname.includes('employee')}>
                  {(handleClick, open) => {
                    return (
                        <React.Fragment>
                          <a href="#0" className={`block text-gray-200 hover:text-white truncate transition duration-150 ${pathname.includes('employee') && 'hover:text-gray-200'}`} onClick={(e) => { e.preventDefault(); sidebarExpanded ? handleClick() : setSidebarExpanded(true) }}>
                            <div className="flex items-center justify-between">
                              <div className="flex items-center">
                                <svg className="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                  <path className={`fill-current text-gray-600 ${pathname.includes('employee') && 'text-indigo-500'}`} d="M18.974 8H22a2 2 0 012 2v6h-2v5a1 1 0 01-1 1h-2a1 1 0 01-1-1v-5h-2v-6a2 2 0 012-2h.974zM20 7a2 2 0 11-.001-3.999A2 2 0 0120 7zM2.974 8H6a2 2 0 012 2v6H6v5a1 1 0 01-1 1H3a1 1 0 01-1-1v-5H0v-6a2 2 0 012-2h.974zM4 7a2 2 0 11-.001-3.999A2 2 0 014 7z" />
                                  <path className={`fill-current text-gray-400 ${pathname.includes('employee') && 'text-indigo-300'}`} d="M12 6a3 3 0 110-6 3 3 0 010 6zm2 18h-4a1 1 0 01-1-1v-6H6v-6a3 3 0 013-3h6a3 3 0 013 3v6h-3v6a1 1 0 01-1 1z" />
                                </svg>
                                <span className="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Karyawan</span>
                              </div>
                              {/* Icon */}
                              <div className="flex shrink-0 ml-2">
                                <svg className={`w-3 h-3 shrink-0 ml-1 fill-current text-gray-400 ${open && 'transform rotate-180'}`} viewBox="0 0 12 12">
                                  <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                </svg>
                              </div>
                            </div>
                          </a>
                          <div className="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul className={`pl-9 mt-1 ${!open && 'hidden'}`}>
                              <li className="mb-1 last:mb-0">
                                <NavLink exact to="/employee/request-verification" className={`block ${pathname.includes('employee/request-verification') && 'text-indigo-500'} text-white hover:text-gray-400 transition duration-150 truncate`}>
                                  <span className="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">- Permintaan Verifikasi <br /> <span className="ml-3">Karyawan Baru</span></span>
                                </NavLink>
                              </li>
                              <li className="mb-1 last:mb-0">
                                <NavLink exact to="/employee/list" className={`block ${pathname.includes('employee/list') && 'text-indigo-500'} text-white hover:text-gray-400 transition duration-150 truncate`}>
                                  <span className="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">- Data Karwayan</span>
                                </NavLink>
                              </li>
                              <li className="mb-1 last:mb-0">
                                <NavLink exact to="/employee/request-delete" className={`block ${pathname.includes('employee/request-delete') && 'text-indigo-500'} text-white hover:text-gray-400 transition duration-150 truncate`}>
                                  <span className="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">- Permintaan Keluar <br /> <span className="ml-3">Perusahaan</span></span>
                                </NavLink>
                              </li>
                            </ul>
                          </div>
                        </React.Fragment>
                    );
                  }}
                </SidebarLinkGroup>
                <SidebarLinkGroup activecondition={pathname.includes('medical-checkup')}>
                  {(handleClick, open) => {
                    return (
                        <React.Fragment>
                          <a href="#0" className={`block text-gray-200 hover:text-white truncate transition duration-150 ${pathname.includes('medical-checkup') && 'hover:text-gray-200'}`} onClick={(e) => { e.preventDefault(); sidebarExpanded ? handleClick() : setSidebarExpanded(true) }}>
                            <div className="flex items-center justify-between">
                              <div className="flex items-center">
                                <svg className="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                  <circle className={`fill-current text-gray-400 ${pathname.includes('medical-checkup') && 'text-indigo-300'}`} cx="18.5" cy="5.5" r="4.5" />
                                  <circle className={`fill-current text-gray-600 ${pathname.includes('medical-checkup') && 'text-indigo-500'}`} cx="5.5" cy="5.5" r="4.5" />
                                  <circle className={`fill-current text-gray-600 ${pathname.includes('medical-checkup') && 'text-indigo-500'}`} cx="18.5" cy="18.5" r="4.5" />
                                  <circle className={`fill-current text-gray-400 ${pathname.includes('medical-checkup') && 'text-indigo-300'}`} cx="5.5" cy="18.5" r="4.5" />
                                </svg>
                                <span className="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Medical Checkup</span>
                              </div>
                              {/* Icon */}
                              <div className="flex shrink-0 ml-2">
                                <svg className={`w-3 h-3 shrink-0 ml-1 fill-current text-gray-400 ${open && 'transform rotate-180'}`} viewBox="0 0 12 12">
                                  <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                </svg>
                              </div>
                            </div>
                          </a>
                          <div className="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul className={`pl-9 mt-1 ${!open && 'hidden'}`}>
                              <li className="mb-1 last:mb-0">
                                <NavLink exact to="/medical-checkup/list" className={`block ${pathname.includes('medical-checkup/list') && 'text-indigo-500'} text-white hover:text-gray-400 transition duration-150 truncate`}>
                                  <span className="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">- Data Medical Checkup</span>
                                </NavLink>
                              </li>
                            </ul>
                          </div>
                        </React.Fragment>
                    );
                  }}
                </SidebarLinkGroup>
                <SidebarLinkGroup activecondition={pathname.includes('company')}>
                  {(handleClick, open) => {
                    return (
                        <div className={`${companyData.companyName === "Bersama Indonesia Sehat Alami" ? '' : 'hidden'}`}>
                          <React.Fragment>
                            <a href="#0" className={`block text-gray-200 hover:text-white truncate transition duration-150 ${pathname.includes('company') && 'hover:text-gray-200'}`} onClick={(e) => { e.preventDefault(); sidebarExpanded ? handleClick() : setSidebarExpanded(true) }}>
                              <div className="flex items-center justify-between">
                                <div className="flex items-center">
                                <svg className="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                  <path
                                    className={`fill-current text-slate-400 ${pathname.includes('company') && 'text-indigo-500'}`}
                                    d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z"
                                  />
                                  <path
                                    className={`fill-current text-slate-700 ${pathname.includes('company') && '!text-indigo-600'}`}
                                    d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z"
                                  />
                                  <path
                                    className={`fill-current text-slate-600 ${pathname.includes('company') && 'text-indigo-500'}`}
                                    d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z"
                                  />
                                </svg>
                                <span className="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Company</span>
                                </div>
                                {/* Icon */}
                                <div className="flex shrink-0 ml-2">
                                  <svg className={`w-3 h-3 shrink-0 ml-1 fill-current text-gray-400 ${open && 'transform rotate-180'}`} viewBox="0 0 12 12">
                                    <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                  </svg>
                                </div>
                              </div>
                            </a>
                            <div className="lg:hidden lg:sidebar-expanded:block 2xl:block">
                              <ul className={`pl-9 mt-1 ${!open && 'hidden'}`}>
                                <li className="mb-1 last:mb-0">
                                  <NavLink exact to="/company/list" className={`block ${pathname.includes('company/list') && 'text-indigo-500'} text-white hover:text-gray-400 transition duration-150 truncate`}>
                                    <span className="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">- List Company</span>
                                  </NavLink>
                                </li>
                              </ul>
                            </div>
                          </React.Fragment>
                        </div>
                    );
                  }}
                </SidebarLinkGroup>
                <SidebarLinkGroup activecondition={pathname.includes('partnership')}>
                  {(handleClick, open) => {
                    return (
                        <div className={`${companyId === 'BISA' ? '' : 'hidden'}`}>
                          <React.Fragment>
                            <a href="#0" className={`block text-gray-200 hover:text-white truncate transition duration-150 ${pathname.includes('partnership') && 'hover:text-gray-200'}`} onClick={(e) => { e.preventDefault(); sidebarExpanded ? handleClick() : setSidebarExpanded(true) }}>
                              <div className="flex items-center justify-between">
                                <div className="flex items-center">
                                  <svg className="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                    <circle className={`fill-current text-gray-400 ${pathname.includes('partnership') && 'text-indigo-300'}`} cx="18.5" cy="5.5" r="4.5" />
                                    <circle className={`fill-current text-gray-600 ${pathname.includes('partnership') && 'text-indigo-500'}`} cx="5.5" cy="5.5" r="4.5" />
                                    <circle className={`fill-current text-gray-600 ${pathname.includes('partnership') && 'text-indigo-500'}`} cx="18.5" cy="18.5" r="4.5" />
                                    <circle className={`fill-current text-gray-400 ${pathname.includes('partnership') && 'text-indigo-300'}`} cx="5.5" cy="18.5" r="4.5" />
                                  </svg>
                                  <span className="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Partnership</span>
                                </div>
                                {/* Icon */}
                                <div className="flex shrink-0 ml-2">
                                  <svg className={`w-3 h-3 shrink-0 ml-1 fill-current text-gray-400 ${open && 'transform rotate-180'}`} viewBox="0 0 12 12">
                                    <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                  </svg>
                                </div>
                              </div>
                            </a>
                            <div className="lg:hidden lg:sidebar-expanded:block 2xl:block">
                              <ul className={`pl-9 mt-1 ${!open && 'hidden'}`}>
                                <li className="mb-1 last:mb-0">
                                  <NavLink exact to="/partnership/list" className={`block ${pathname.includes('partnership/list') && 'text-indigo-500'} hover:text-gray-200 transition duration-150 truncate`}>
                                    <span className="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Partner</span>
                                  </NavLink>
                                </li>
                              </ul>
                            </div>
                          </React.Fragment>
                        </div>
                    );
                  }}
                </SidebarLinkGroup>
              </ul>
            </div>
          </div>

          <div className="pt-3 hidden lg:inline-flex 2xl:hidden justify-end mt-auto">
            <div className="px-3 py-2">
              <button onClick={() => setSidebarExpanded(!sidebarExpanded)}>
                <span className="sr-only">Expand / collapse sidebar</span>
                <svg className="w-6 h-6 fill-current sidebar-expanded:rotate-180" viewBox="0 0 24 24">
                  <path className="text-gray-400" d="M19.586 11l-5-5L16 4.586 23.414 12 16 19.414 14.586 18l5-5H7v-2z" />
                  <path className="text-gray-600" d="M3 23H1V1h2z" />
                </svg>
              </button>
            </div>
          </div>

        </div>
      </div>
  );
}

export default Sidebar;