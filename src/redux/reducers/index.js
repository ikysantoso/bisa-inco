import { combineReducers } from "redux";
import { companyReducer } from "./companyReducer";
import { userReducer } from "./userReducer";

const reducers = combineReducers({
    company: companyReducer,
    user: userReducer,
});

export default reducers;