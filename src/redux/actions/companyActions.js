import {ActionTypes} from "../constants/action-types";

export const setCompany = (company) => {
    return {
        type: ActionTypes.SET_COMPANY,
        payload: company,
    };
};