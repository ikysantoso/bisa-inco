import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({...rest}) => {
    return (
        localStorage.getItem('id') ? <Route {...rest} /> : <Redirect to='/login' />
    );
};

export default PrivateRoute;